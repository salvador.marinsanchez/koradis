// Copyright Epic Games, Inc. All Rights Reserved.


#include "AureiStarGameModeBase.h"

void AAureiStarGameModeBase::WorldChanged(bool toAqualis)
{
	isAqualisWorld = toAqualis;

	if (isAqualisWorld) {
		ToAquaWorldEvent.Broadcast();
	}
	else {
		ToNormalWorldEvent.Broadcast();
	}
}
