// Fill out your copyright notice in the Description page of Project Settings.


#include "AureiStar/Public/Projectiles/ProjectilePoolComponent.h"
//Projectile types
#include "AureiStar/Public/Projectiles/Actors/ProjClimb.h"
#include "AureiStar/Public/Projectiles/Actors/ProjFlyingMelee.h"
#include "AureiStar/Public/Projectiles/Actors/ProjFlyingRange.h"
#include "AureiStar/Public/Projectiles/Actors/ProjGroundMelee.h"
#include "AureiStar/Public/Projectiles/Actors/ProjGroundRange.h"
#include "AureiStar/Public/Projectiles/Actors/ProjWallRun.h"

// Sets default values for this component's properties
UProjectilePoolComponent::UProjectilePoolComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UProjectilePoolComponent::BeginPlay()
{
	Super::BeginPlay();

	initClasses();
	initPools();
}


void UProjectilePoolComponent::initClasses()
{

	for (EProjectileTypes val : TEnumRange<EProjectileTypes>()) {
		if (val == EProjectileTypes::HEAL) continue;

		TSubclassOf<AProjectileBase> *classToSpawn = enumToClass.Find(val);

		if (classToSpawn == nullptr) {
			UClass* test = nullptr;

			if (val == EProjectileTypes::CLIMB)
				test = AProjClimb::StaticClass();
			else if (val == EProjectileTypes::FLY_MELEE)
				test = AProjFlyingMelee::StaticClass();
			else if (val == EProjectileTypes::FLY_RANGE)
				test = AProjFlyingRange::StaticClass();
			else if (val == EProjectileTypes::GROUND_MELEE)
				test = AProjGroundMelee::StaticClass();
			else if (val == EProjectileTypes::GROUND_RANGE)
				test = AProjGroundRange::StaticClass();
			else if (val == EProjectileTypes::WALLRUN)
				test = AProjWallRun::StaticClass();
			
			enumToClass.Add(val, (TSubclassOf<AProjectileBase>) test);
		}
	}
}


/*
* The following functions with huge switches could be improved, but they are only used at the start of every game and rarely after that, so its probably not 
* worth to bother
*/

void UProjectilePoolComponent::initPools()
{
	//CLIMB PROJECTILES
	for (EProjectileTypes val : TEnumRange<EProjectileTypes>()) {
		if (val == EProjectileTypes::HEAL) continue;

		int* limit = initialPoolSize.Find(val);

		for (int i = 0; i < *limit; ++i) {
			CreateProjectile(val);
		}
	}
}

void UProjectilePoolComponent::CreateProjectile(EProjectileTypes type) {
	UClass *classToSpawn = *enumToClass.Find(type);

	switch (type) {
	case EProjectileTypes::CLIMB:{
		AProjClimb* p = Cast<AProjClimb>(GetWorld()->SpawnActor(classToSpawn));
		p->SetPool(this);

		Climbs.Push(p);
		break;
	}

	case EProjectileTypes::FLY_MELEE:{
		AProjFlyingMelee* p = Cast<AProjFlyingMelee>(GetWorld()->SpawnActor(classToSpawn));
		p->SetPool(this);

		FlyMelees.Push(p);
		break;
	}

	case EProjectileTypes::FLY_RANGE:{
		AProjFlyingRange* p = Cast<AProjFlyingRange>(GetWorld()->SpawnActor(classToSpawn));
		p->SetPool(this);

		FlyRanges.Push(p);
		break;
	}
	case EProjectileTypes::GROUND_MELEE:{
		AProjGroundMelee* p = Cast<AProjGroundMelee>(GetWorld()->SpawnActor(classToSpawn));
		p->SetPool(this);

		GroundMelees.Push(p);
		break;
	}

	case EProjectileTypes::GROUND_RANGE:{
		AProjGroundRange* p = Cast<AProjGroundRange>(GetWorld()->SpawnActor(classToSpawn));
		p->SetPool(this);

		GroundRanges.Push(p);
		break;
	}

	case EProjectileTypes::WALLRUN:{
		AProjWallRun* p = Cast<AProjWallRun>(GetWorld()->SpawnActor(classToSpawn));
		p->SetPool(this);

		WallRuns.Push(p);
		break;
	}

	default:{
		GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Red, FString::Printf(TEXT("CREATE PROJECTILE WITH AN UNVALID PROJECTILE CALLED")));
		break;
	}
	}
}

AProjectileBase* UProjectilePoolComponent::GetOrCreateProjectile(EProjectileTypes type, TArray<AProjectileBase*>& arr)
{
	if (arr.Num() == 0) {
		CreateProjectile(type);
	}
	
	return arr.Pop();
}

AProjectileBase* UProjectilePoolComponent::GetProjectile(EProjectileTypes type) {
	AProjectileBase* output = nullptr;

	switch (type) {
	case EProjectileTypes::CLIMB: {
		output = GetOrCreateProjectile(type, (TArray<AProjectileBase*>&) Climbs);
		
		break;
	}

	case EProjectileTypes::FLY_MELEE: {
		output = GetOrCreateProjectile(type, (TArray<AProjectileBase*>&) FlyMelees);

		break;
	}

	case EProjectileTypes::FLY_RANGE: {
		output = GetOrCreateProjectile(type, (TArray<AProjectileBase*>&) FlyRanges);

		break;
	}
	case EProjectileTypes::GROUND_MELEE: {
		output = GetOrCreateProjectile(type, (TArray<AProjectileBase*>&) GroundMelees);

		break;
	}

	case EProjectileTypes::GROUND_RANGE: {
		output = GetOrCreateProjectile(type, (TArray<AProjectileBase*>&) GroundRanges);
		
		break;
	}

	case EProjectileTypes::WALLRUN: {
		output = GetOrCreateProjectile(type, (TArray<AProjectileBase*>&) WallRuns);
		
		break;
	}

	default: {
		GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Red, FString::Printf(TEXT("GET PROJECTILE WITH AN UNVALID PROJECTILE CALLED")));
		break;
	}
	}

	return output;
}

void UProjectilePoolComponent::AddProjectile(AProjectileBase* actor, EProjectileTypes type) {
	switch (type) {
	case EProjectileTypes::CLIMB: {
		Climbs.Push(Cast<AProjClimb>(actor));
		break;
	}

	case EProjectileTypes::FLY_MELEE: {
		FlyMelees.Push(Cast<AProjFlyingMelee>(actor));
		break;
	}

	case EProjectileTypes::FLY_RANGE: {
		FlyRanges.Push(Cast<AProjFlyingRange>(actor));
		break;
	}
	case EProjectileTypes::GROUND_MELEE: {
		GroundMelees.Push(Cast<AProjGroundMelee>(actor));
		break;
	}

	case EProjectileTypes::GROUND_RANGE: {
		GroundRanges.Push(Cast<AProjGroundRange>(actor));
		break;
	}

	case EProjectileTypes::WALLRUN: {
		WallRuns.Push(Cast<AProjWallRun>(actor));
		break;
	}

	default: {
		GEngine->AddOnScreenDebugMessage(0, 10.f, FColor::Red, FString::Printf(TEXT("ADD PROJECTILE WITH AN UNVALID PROJECTILE CALLED")));
		break;
	}
	}
}
