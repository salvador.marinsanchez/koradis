// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjClimb.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

AProjClimb::AProjClimb()
{
	PrimaryActorTick.bCanEverTick = true;

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	sphere->SetEnableGravity(false);
	SetRootComponent(sphere);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(sphere);
	mesh->SetEnableGravity(false);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCanEverAffectNavigation(false);

	ptype = EProjectileTypes::CLIMB;
}

void AProjClimb::Shoot(AActor* target, FVector direction)
{
	if (target == nullptr) {
		timerRunsOut();
	}
	else {
		movComp->HomingTargetComponent = target->GetRootComponent();
		movComp->bIsHomingProjectile = true;

		direction = target->GetActorLocation() - GetActorLocation();

		direction.Normalize();
		movComp->Velocity = direction * movComp->InitialSpeed;
		SetActorRotation(direction.Rotation());

		Super::Shoot(target, direction);
	}
}

void AProjClimb::OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != UGameplayStatics::GetPlayerPawn(GetWorld(), 0)) {

		doAoE(OtherActor);
		timerRunsOut();
	}
}



void AProjClimb::BeginPlay()
{
	Super::BeginPlay();

	sphere->OnComponentBeginOverlap.AddDynamic(this, &AProjClimb::OnSphereOverlap);
}

void AProjClimb::doAoE(AActor* hitActor)
{
	DrawDebugSphere(GetWorld(), GetActorLocation(), AoEradius, 32, FColor::Red, false, 2.f);

	TArray<AActor*> actorsToIgnore;
	actorsToIgnore.Add(GetOwner());

	TArray<TEnumAsByte<EObjectTypeQuery>> objects;
	objects.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

	TArray<UPrimitiveComponent*> output;

	UKismetSystemLibrary::SphereOverlapComponents(GetWorld(), GetActorLocation(), AoEradius, objects, false, actorsToIgnore, output);

	for (UPrimitiveComponent* comp : output) {
		if (comp->ComponentHasTag("EnemyBody")) {

			if (hitActor == comp->GetOwner()) {
				UGameplayStatics::ApplyDamage(comp->GetOwner(), baseDamage, nullptr, this, damageType);
			}else{
				float distToCenter = FVector::Dist(GetActorLocation(), comp->GetOwner()->GetActorLocation());
		
				float radiusMultiplier = RadiusDamageFallOffCurve->GetFloatValue(distToCenter / AoEradius);

				UGameplayStatics::ApplyDamage(comp->GetOwner(), baseDamage * radiusMultiplier, nullptr, this, damageType);
			}
		}
	}
}
