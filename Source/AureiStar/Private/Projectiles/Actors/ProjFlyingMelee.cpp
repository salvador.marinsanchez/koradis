// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjFlyingMelee.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

AProjFlyingMelee::AProjFlyingMelee()
{
	PrimaryActorTick.bCanEverTick = true;

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	sphere->SetEnableGravity(false);
	SetRootComponent(sphere);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(sphere);
	mesh->SetEnableGravity(false);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCanEverAffectNavigation(false);

	ptype = EProjectileTypes::FLY_MELEE;
}

void AProjFlyingMelee::Shoot(AActor* target, FVector direction)
{
	if (target == nullptr) {
		timerRunsOut();
	}
	else {
		movComp->HomingTargetComponent = target->GetRootComponent();
		movComp->bIsHomingProjectile = true;

		direction = target->GetActorLocation() - GetActorLocation();

		direction.Normalize();
		movComp->Velocity = direction * movComp->InitialSpeed;
		SetActorRotation(direction.Rotation());

		attachedActor = nullptr;

		Super::Shoot(target, direction);
	}
}

void AProjFlyingMelee::OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isValidTarget(OtherComp, OtherActor)) {

		//Stop the movement and the "autoDestroy" timer.
		if (despawnTimerHandle.IsValid())
		{
			GetWorldTimerManager().ClearTimer(despawnTimerHandle);
			despawnTimerHandle.Invalidate();
		}

		movComp->StopMovementImmediately();
		movComp->bIsHomingProjectile = false;
		movComp->HomingTargetComponent = nullptr;

		//Attach the projectile to the target
		AttachToActor(OtherActor, FAttachmentTransformRules::KeepWorldTransform);
		SetActorEnableCollision(false);
		attachedActor = OtherActor;

		//Start timer to explode and do damage
		GetWorldTimerManager().SetTimer(explosionTimerHandle, this, &AProjFlyingMelee::ExplosionTimerRunsOut, timeToExplode, false);
	}
}

//Static parameters must be initialized in the cpp file
static float cumDamage = 0;
static FTimerHandle resetCumDamageHandle;

void AProjFlyingMelee::ExplosionTimerRunsOut()
{
	UGameplayStatics::ApplyDamage(attachedActor, baseDamage + cumDamage, nullptr, this, damageType);
	cumDamage += damageIncreasePerShot;

	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	//Start timer to reset cumDamage
	if (resetCumDamageHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(resetCumDamageHandle);
		resetCumDamageHandle.Invalidate();
	}
	
	GetWorldTimerManager().SetTimer(resetCumDamageHandle, [this]() {cumDamage = 0; }, cooldown*2, false);

	timerRunsOut();
}



void AProjFlyingMelee::BeginPlay()
{
	Super::BeginPlay();

	sphere->OnComponentBeginOverlap.AddDynamic(this, &AProjFlyingMelee::OnSphereOverlap);
}