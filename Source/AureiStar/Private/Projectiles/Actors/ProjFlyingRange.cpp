// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjFlyingRange.h"


#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Projectiles/CustomDamageTypes.h"


AProjFlyingRange::AProjFlyingRange()
{
	PrimaryActorTick.bCanEverTick = true;

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	sphere->SetEnableGravity(false);
	SetRootComponent(sphere);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(sphere);
	mesh->SetEnableGravity(false);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCanEverAffectNavigation(false);

	ptype = EProjectileTypes::FLY_RANGE;
}

void AProjFlyingRange::Shoot(AActor* target, FVector direction)
{
	if (target == nullptr) {
		timerRunsOut();
	}
	else {
		movComp->HomingTargetComponent = target->GetRootComponent();
		movComp->bIsHomingProjectile = true;

		direction = target->GetActorLocation() - GetActorLocation();

		direction.Normalize();
		movComp->Velocity = direction * movComp->InitialSpeed;
		SetActorRotation(direction.Rotation());

		Super::Shoot(target, direction);
	}
}

void AProjFlyingRange::OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isValidTarget(OtherComp, OtherActor)) {

		//Stop the movement and the "autoDestroy" timer.
		if (despawnTimerHandle.IsValid())
		{
			GetWorldTimerManager().ClearTimer(despawnTimerHandle);
			despawnTimerHandle.Invalidate();
		}

		movComp->StopMovementImmediately();
		movComp->bIsHomingProjectile = false;
		movComp->HomingTargetComponent = nullptr;

		//Attach the projectile to the target
		AttachToActor(OtherActor, FAttachmentTransformRules::KeepWorldTransform);
		SetActorEnableCollision(false);
		attachedActor = OtherActor;

		//Start timer to despawn the debuf
		GetWorldTimerManager().SetTimer(dotDurationHandle, this, &AProjFlyingRange::debufDurationFinished, debufDuration, false);
		//Start timer to do dot damage
		GetWorldTimerManager().SetTimer(dotTickHandle, this, &AProjFlyingRange::debufTickCallback, tickDuration, true);
	}
}

void AProjFlyingRange::debufDurationFinished()
{
	//Stop the dot timer.
	if (dotTickHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(dotTickHandle);
		dotTickHandle.Invalidate();
	}

	timerRunsOut();
}

void AProjFlyingRange::debufTickCallback()
{
	UGameplayStatics::ApplyDamage(attachedActor, damageOnTick, nullptr, this, UDoT_DamageType::StaticClass());
}



void AProjFlyingRange::BeginPlay()
{
	Super::BeginPlay();

	sphere->OnComponentBeginOverlap.AddDynamic(this, &AProjFlyingRange::OnSphereOverlap);
}
