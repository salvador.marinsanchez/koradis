// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjWallRun.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

AProjWallRun::AProjWallRun()
{
	initialBaseDamage = baseDamage;
	PrimaryActorTick.bCanEverTick = true;

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	sphere->SetEnableGravity(false);
	SetRootComponent(sphere);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(sphere);
	mesh->SetEnableGravity(false);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCanEverAffectNavigation(false);

	ptype = EProjectileTypes::WALLRUN;
}

void AProjWallRun::Shoot(AActor* target, FVector direction)
{
	if (target == nullptr) {
		timerRunsOut();
	}
	else {
		movComp->HomingTargetComponent = target->GetRootComponent();
		movComp->bIsHomingProjectile = true;

		direction = target->GetActorLocation() - GetActorLocation();

		direction.Normalize();
		movComp->Velocity = direction * movComp->InitialSpeed;
		SetActorRotation(direction.Rotation());

		Super::Shoot(target, direction);
	}
}

void AProjWallRun::OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isValidTarget(OtherComp, OtherActor)) {

		UGameplayStatics::ApplyDamage(OtherActor, baseDamage, nullptr, this, damageType);

		timerRunsOut();
	}
}



void AProjWallRun::BeginPlay()
{
	Super::BeginPlay();
	baseDamage = initialBaseDamage;

	sphere->OnComponentBeginOverlap.AddDynamic(this, &AProjWallRun::OnSphereOverlap);
}
