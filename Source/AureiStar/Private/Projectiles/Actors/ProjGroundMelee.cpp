// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjGroundMelee.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/DamageEvents.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"

AProjGroundMelee::AProjGroundMelee() {
	PrimaryActorTick.bCanEverTick = true;

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	sphere->SetEnableGravity(false);
	SetRootComponent(sphere);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(sphere);
	mesh->SetEnableGravity(false);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCanEverAffectNavigation(false);

	ptype = EProjectileTypes::GROUND_MELEE;
}

void AProjGroundMelee::Shoot(AActor* target, FVector direction)
{
	direction.Normalize();
	movComp->Velocity = direction * movComp->InitialSpeed;
	SetActorRotation(direction.Rotation());
	actorsHit.Empty();

	Super::Shoot(target, direction);
}

void AProjGroundMelee::OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isValidTarget(OtherComp, OtherActor) && !actorsHit.Contains(OtherActor) ){
		//This projectile does damage to any enemy it goes trought
		actorsHit.Add(OtherActor);
		//TODO: DO DAMAGE TO THE ENEMY


		UGameplayStatics::ApplyDamage(OtherActor, baseDamage, nullptr, this, damageType);
	}
}



void AProjGroundMelee::BeginPlay()
{
	Super::BeginPlay();

	sphere->OnComponentBeginOverlap.AddDynamic(this, &AProjGroundMelee::OnSphereOverlap);
}




