// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjectileBase.h"
#include "Projectiles/ProjectilePoolComponent.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "GameFramework/DamageType.h"
#include "AureiStar/Public/Enemies/EnemyPawnBase.h"

AProjectileBase::AProjectileBase()
{
	PrimaryActorTick.bCanEverTick = true;

	movComp = CreateDefaultSubobject<UProjectileMovementComponent >(TEXT("Projectile Movement"));
	movComp->ProjectileGravityScale = 0;
	movComp->Velocity = FVector::Zero();
	movComp->bRotationFollowsVelocity = true;

}

void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
}

bool AProjectileBase::isValidTarget(UPrimitiveComponent* comp, AActor* actor)
{
	return comp->ComponentHasTag("EnemyBody") && !Cast<AEnemyPawnBase>(actor)->isDead();
}

void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AProjectileBase::getCooldown()
{
	return cooldown;
}

void AProjectileBase::Shoot(AActor *target, FVector direction)
{
	SetActorHiddenInGame(false);
	SetActorEnableCollision(true);

	GetWorldTimerManager().SetTimer(despawnTimerHandle, this, &AProjectileBase::timerRunsOut, secondsToDespawn, false);

}

void AProjectileBase::SetPool(UProjectilePoolComponent* pl)
{
	pool = pl;
}

void AProjectileBase::ReturnToPool(EProjectileTypes type)
{
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);

	pool->AddProjectile(this, type);
}

void AProjectileBase::timerRunsOut()
{
	if (despawnTimerHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(despawnTimerHandle);
		despawnTimerHandle.Invalidate();
	}

	movComp->StopMovementImmediately();
	movComp->bIsHomingProjectile = false;
	movComp->HomingTargetComponent = nullptr;

	ReturnToPool(ptype);
}