// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectiles/Actors/ProjGroundRange.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

AProjGroundRange::AProjGroundRange()
{
	initialBaseDamage = baseDamage;
	PrimaryActorTick.bCanEverTick = true;

	sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	sphere->SetEnableGravity(false);
	SetRootComponent(sphere);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(sphere);
	mesh->SetEnableGravity(false);
	mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	mesh->SetCanEverAffectNavigation(false);

	ptype = EProjectileTypes::GROUND_RANGE;
}

void AProjGroundRange::Shoot(AActor* target, FVector direction)
{
	if (target == nullptr) {
		timerRunsOut();
	}
	else {

	
	direction = target->GetActorLocation() - GetActorLocation();

	direction.Normalize();
	movComp->Velocity = direction * movComp->InitialSpeed;

	SetActorRotation(direction.Rotation());
	actorsHit.Empty();

	baseDamage = initialBaseDamage;

	Super::Shoot(target, direction);
	}
}

void AProjGroundRange::OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isValidTarget(OtherComp, OtherActor) && !actorsHit.Contains(OtherActor)) {
		//This projectile does damage to any enemy it goes trought
		actorsHit.Add(OtherActor);

		UGameplayStatics::ApplyDamage(OtherActor, baseDamage, nullptr, this, damageType);

		//Reduce damage dealt after each enemy hit
		baseDamage *= damageDropOffmultiplier;
	}
}



void AProjGroundRange::BeginPlay()
{
	Super::BeginPlay();

	sphere->OnComponentBeginOverlap.AddDynamic(this, &AProjGroundRange::OnSphereOverlap);
}




