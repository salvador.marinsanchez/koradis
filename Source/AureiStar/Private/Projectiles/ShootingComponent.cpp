
#include "AureiStar/Public/Projectiles/ShootingComponent.h"
#include "AureiStar/Public/Projectiles/ProjectilePoolComponent.h"
#include "AureiStar/Public/Enemies/EnemyPawnBase.h"
#include "AureiStar/Public/Components/GetCloseEnemies.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/SphereComponent.h"
#include "AureiStar/Public/Components/CameraTargetFixComponent.h"


UShootingComponent::UShootingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UShootingComponent::BeginPlay()
{
	Super::BeginPlay();

	projPool = GetOwner()->GetComponentByClass<UProjectilePoolComponent>();
	check(projPool != nullptr);

	moveComp = GetOwner()->GetComponentByClass<UAureiCharacterMovementComponent>();
	check(moveComp != nullptr);

	getCloseComp = GetOwner()->GetComponentByClass<UGetCloseEnemies>();
	check(getCloseComp != nullptr);
	
	cameraFixComp = GetOwner()->GetComponentByClass<UCameraTargetFixComponent>();
	check(cameraFixComp != nullptr);

	moveComp->MovementModeChangedEvent.AddDynamic(this, &UShootingComponent::onMovementStateChanged);
}


void UShootingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (automaticShoot) {
		TryShoot();
	}
}

void UShootingComponent::onMovementStateChanged(EMovementMode moveMode, ECustomMovementMode custMode)
{
	float* cd = moveMode == EMovementMode::MOVE_Custom ?
		CustomMovModeChange_Cooldown.Find(custMode) :
		MovModeChange_Cooldown.Find(moveMode);

	if (cd != nullptr) {
		startCooldown(*cd);
	}
}

void UShootingComponent::TryShoot()
{
	if (isShootAvailable()) {
		AActor* closestActor = cameraFixComp->GetFixatedEnemy(); 
		if(closestActor == nullptr) closestActor = getCloseComp->getClosestEnemy();

		if (closestActor != nullptr) {

			bool useRangeProj = false;

			if (closestActor != nullptr && FVector::Dist2D(closestActor->GetActorLocation(), GetOwner()->GetActorLocation()) > distanceToRangeAttack) {
				useRangeProj = true;
			}

			EProjectileTypes proj = useRangeProj ? EProjectileTypes::GROUND_RANGE : EProjectileTypes::GROUND_MELEE;

			if (moveComp->IsCustomMovementMode(ECustomMovementMode::CMOVE_Climb)) {
				proj = EProjectileTypes::CLIMB;
			}
			else if (moveComp->IsCustomMovementMode(ECustomMovementMode::CMOVE_WallRun)) {
				proj = EProjectileTypes::WALLRUN;
			}
			else if (moveComp->IsMovementMode(EMovementMode::MOVE_Flying)) {
				proj = useRangeProj ? EProjectileTypes::FLY_RANGE : EProjectileTypes::FLY_MELEE;
			}

			AProjectileBase* projActor = projPool->GetProjectile(proj);
			startCooldown(projActor->getCooldown());

			projActor->SetActorLocation(GetOwner()->GetActorLocation());
			projActor->Shoot(closestActor, GetOwner()->GetActorForwardVector());
		}
	}
}

bool UShootingComponent::isShootAvailable()
{
	return canShoot;
}

void UShootingComponent::startCooldown(float cd)
{
	if (cd > 0) {
		canShoot = false;

		GetWorld()->GetTimerManager().ClearTimer(cooldownTimerHandle);
		cooldownTimerHandle.Invalidate();

		GetWorld()->GetTimerManager().SetTimer(cooldownTimerHandle, [this]() {canShoot = true; }, cd, false);
	}
}

