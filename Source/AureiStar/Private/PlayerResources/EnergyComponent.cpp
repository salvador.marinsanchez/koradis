// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerResources/EnergyComponent.h"

UEnergyComponent::UEnergyComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UEnergyComponent::BeginPlay()
{
	Super::BeginPlay();

	currentEnergy = maxEnergy;
}


void UEnergyComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (state == EEnergyState::Idle) return;

	if (state == EEnergyState::Regenerating) {
		if (currentEnergy >= maxEnergy) {
			ChangeEnergyState(EEnergyState::Idle);
		}
		else {
			ChangeEnergy(currentEnergyRegen * DeltaTime);
		}
	}

	if (state == EEnergyState::Spending) {
		if (currentEnergy <= 0.0f) {
			currentEnergy = 0;
			ChangeEnergyState(EEnergyState::Idle);
		}
		else {
			ChangeEnergy(-currentEnergyRegen * DeltaTime);
		}
	}
}

void UEnergyComponent::ChangeEnergyState(EEnergyState newState, float newEnergyRegen)
{
	if (newState != EEnergyState::Idle) {
		currentEnergyRegen = newEnergyRegen;
	}

	state = newState;
}

void UEnergyComponent::SetAutoRegen(bool newAutoRegen)
{
	isAutoRegenActive = newAutoRegen;

	if (isAutoRegenActive) {
		tryAutoRegen();
	}
	else {
		if (autoRegenHandle.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(autoRegenHandle);
			autoRegenHandle.Invalidate();
		}

		if (state == EEnergyState::Regenerating) {
			ChangeEnergyState(EEnergyState::Idle);
		}
	}

	
}

bool UEnergyComponent::CanStartSpending(float minEnergyTo)
{
	return currentEnergy >= minEnergyTo && !disableSpending;
}

void UEnergyComponent::SetDisableSpending(bool disable)
{
	disableSpending = disable;

	if (disableSpending) {
		spendingDisabledEvent.Broadcast();
	}
	else {
		tryAutoRegen();
	}
}

void UEnergyComponent::ChangeEnergy(float amountToChange)
{
	float oldEnergy = currentEnergy;

	currentEnergy = FMath::Clamp<float>((currentEnergy + amountToChange), 0.0f, maxEnergy);

	if (currentEnergy == 0.f) zeroEnergyEvent.Broadcast();
	energyChangedEvent.Broadcast(oldEnergy, currentEnergy);

	if (amountToChange <= 0) {
		tryAutoRegen();
	}
}

void UEnergyComponent::tryAutoRegen()
{
	if (!isAutoRegenActive) return;

	if (autoRegenHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(autoRegenHandle);
		autoRegenHandle.Invalidate();
	}

	GetWorld()->GetTimerManager().SetTimer(autoRegenHandle, [this]()
		{
			if (!isAutoRegenActive || state == EEnergyState::Regenerating || disableSpending) return;
			else ChangeEnergyState(EEnergyState::Regenerating, AutoRegenEnergy);
		}, delayToStartAutoRegen, false);
}


