#include "PlayerResources/HealthComponent.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	currentHealth = maxHealth;

	for (EProjectileTypes val : TEnumRange<EProjectileTypes>()) {
		float* resistance = damageVulnerability.Find(val);
		if (resistance == nullptr)
		{
			damageVulnerability.Add(val, 1.f);
		}
	}
}


void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (state == EHealthState::stable) return;

	if (state == EHealthState::increasing) {
		if (currentHealth >= maxHealth) {
			ChangeHealthState(EHealthState::stable);
		}
		else {
			ChangeHealth(currentHealthRegen * DeltaTime, EProjectileTypes::HEAL);
		}
	}

	if (state == EHealthState::reducing) {
		if (currentHealth <= 0.0f) {
			currentHealth = 0;
			ChangeHealthState(EHealthState::stable);
		}
		else {
			ChangeHealth(-currentHealthRegen * DeltaTime, currentRegenDamageType);
		}
	}
}

void UHealthComponent::ChangeHealthState(EHealthState newState, EProjectileTypes damageType, float newHealthRegen)
{
	if (newState != EHealthState::stable) {
		currentHealthRegen = newHealthRegen;
	}

	currentRegenDamageType = damageType;

	state = newState;
}

void UHealthComponent::SetAutoRegen(bool newAutoRegen)
{
	isAutoRegenActive = newAutoRegen;

	if (isAutoRegenActive) {
		tryAutoRegen();
	}
	else {
		if (autoRegenHandle.IsValid())
		{
			GetWorld()->GetTimerManager().ClearTimer(autoRegenHandle);
			autoRegenHandle.Invalidate();
		}

		if (state == EHealthState::increasing) {
			ChangeHealthState(EHealthState::stable);
		}
	}
}

void UHealthComponent::ReceiveDamage(float amount, EProjectileTypes type)
{
	if (currentHealth <= 0) return;
	ChangeHealth(-amount, type);
}

void UHealthComponent::HealDamage(float amount)
{
	ChangeHealth(amount, EProjectileTypes::HEAL);
}

void UHealthComponent::ChangeHealth(float amountToChange, EProjectileTypes type)
{
	float oldHp = currentHealth;

	amountToChange *= *damageVulnerability.Find(type);

	currentHealth = FMath::Clamp<float>((currentHealth + amountToChange), 0.0f, maxHealth);

	if (currentHealth == 0.f) zeroHealthEvent.Broadcast(type);
	healthChangedEvent.Broadcast(oldHp, currentHealth);

	if (amountToChange <= 0) {
		tryAutoRegen();
	}
}

void UHealthComponent::tryAutoRegen()
{
	if (!isAutoRegenActive) return;

	if (autoRegenHandle.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(autoRegenHandle);
		autoRegenHandle.Invalidate();
	}

	ChangeHealthState(EHealthState::stable, EProjectileTypes::HEAL, AutoRegenHealth);

	GetWorld()->GetTimerManager().SetTimer(autoRegenHandle, [this]()
		{
			if (!isAutoRegenActive || state == EHealthState::increasing) return;
			else ChangeHealthState(EHealthState::increasing, EProjectileTypes::HEAL, AutoRegenHealth);
		}, delayToStartAutoRegen, false);
}


