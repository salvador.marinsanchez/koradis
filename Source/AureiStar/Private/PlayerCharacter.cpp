// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"

#include "AureiStar/Public/Projectiles/ShootingComponent.h"
#include "AureiCharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/SphereComponent.h"
#include "AureiStar/Public/PlayerResources/EnergyComponent.h"
#include "AureiStar/Public/PlayerResources/HealthComponent.h"
#include <Kismet/KismetSystemLibrary.h>
#include <Interactuables/CanBeInteracted.h>
#include "AureiStar/Public/Components/GetCloseEnemies.h"
#include <Kismet/GameplayStatics.h>
#include "AureiStar/AureiStarGameModeBase.h"
#include "Components/CameraTargetFixComponent.h"

static FName playerBodyTag = "PlayerBody";

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UAureiCharacterMovementComponent>(ACharacter::CharacterMovementComponentName)) {

	AureiCharacterMovementComponent = Cast<UAureiCharacterMovementComponent>(GetCharacterMovement());
	AureiCharacterMovementComponent->SetIsReplicated(true);

	TurnRateGamepad = 50.f;

	bUseControllerRotationPitch = bUseControllerRotationYaw = bUseControllerRotationRoll = false;

	//Configure character movement
	AureiCharacterMovementComponent->bOrientRotationToMovement = true;
	AureiCharacterMovementComponent->RotationRate = FRotator(0.0f, 500.0f, 0.0f);

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f;
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	EnemiesInRangeSphere = CreateDefaultSubobject<USphereComponent>(TEXT("EnemiesInRange Sphere"));
	EnemiesInRangeSphere->SetupAttachment(RootComponent);

	EnergyComp = CreateDefaultSubobject<UEnergyComponent>(TEXT("Energy Component"));
	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
	getCloseComp = CreateDefaultSubobject<UGetCloseEnemies>(TEXT("GetClosestEnemy Component"));
	cameraTargetFixComp = CreateDefaultSubobject<UCameraTargetFixComponent>(TEXT("UCameraTargetFixComponent Component"));

	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Setting las variables de la clase del movimiento del personaje
	AureiCharacterMovementComponent->bUseControllerDesiredRotation = true;
	AureiCharacterMovementComponent->bIgnoreBaseRotation = true;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	startingTransform = GetActorTransform();

	ShootingComp = GetComponentByClass<UShootingComponent>();
	check(ShootingComp != nullptr);

	getCloseComp->SetSphereComp(EnemiesInRangeSphere);

	bIsGravityEnabled = true;
	bCanGoUp = false;

	EnergyComp->zeroEnergyEvent.AddDynamic(this, &APlayerCharacter::EnergyToZeroCallback);
	EnergyComp->spendingDisabledEvent.AddDynamic(this, &APlayerCharacter::EnergyToZeroCallback);

	HealthComp->zeroHealthEvent.AddDynamic(this, &APlayerCharacter::RespawnPlayer);

	AureiCharacterMovementComponent->WR_CooldownFinishedEvent.AddDynamic(this, &APlayerCharacter::WhenWREnds);

	anims = GetMesh()->GetAnimInstance();
	anims->OnMontageEnded.AddDynamic(this, &APlayerCharacter::OnMontageEnded);

	gm = Cast<AAureiStarGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Llama a la funcion RaiseCharacter en cada fotograma.
	RaiseCharacter();

	if (AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_WallRun)) {
		AureiCharacterMovementComponent->bUseControllerDesiredRotation = false;
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveUp", this, &APlayerCharacter::MoveUp);
	PlayerInputComponent->BindAxis("Turn", this, &APlayerCharacter::TurnCamera);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::LookUpCamera);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &APlayerCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &APlayerCharacter::EndCrouch);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerCharacter::BeginSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::EndSprint);
	PlayerInputComponent->BindAction("Fly", IE_Pressed, this, &APlayerCharacter::Fly);
	PlayerInputComponent->BindAction("Dash", IE_Pressed, this, &APlayerCharacter::Dashing);
	PlayerInputComponent->BindAction("GroundP", IE_Pressed, this, &APlayerCharacter::GroundPound);
	
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &APlayerCharacter::ShootPressed);
	
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &APlayerCharacter::InteractPressed);

	PlayerInputComponent->BindAction("Fixate", IE_Pressed, this, &APlayerCharacter::FixateEnemy);

	
	PlayerInputComponent->BindTouch(IE_Pressed, this, &APlayerCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &APlayerCharacter::TouchStopped);
}

void APlayerCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location) {
	Jump();
}

void APlayerCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location) {
	StopJumping();
}

void APlayerCharacter::Jump() {
	bPressedAureiJump = true;

	Super::Jump();

	bPressedJump = false;
}

void APlayerCharacter::StopJumping() {
	bPressedAureiJump = false;
	Super::StopJumping();
}

void APlayerCharacter::MoveForward(float AxisValue) {
	if ((Controller != nullptr) && (AxisValue != 0.0f)) {

		if(!AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_WallRun)){
			//Buscar que direcci�n es hacia adelante
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			//Obtener el vector hacia adelante
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

			//A�adir movimiento a esa direcci�n
			AddMovementInput(Direction, AxisValue);
			return;
		}
	}
}

void APlayerCharacter::MoveRight(float AxisValue) {
	if (Controller != nullptr && AxisValue != 0.0f) {

		if (AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_Climb)) {

			//AddMovementInput(FVector::RightVector, AxisValue);
			return;
		}

		if (!AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_WallRun)) {
			//Buscar que direcci�n es hacia la derecha
			const FRotator Rotation = Controller->GetControlRotation();
			const FRotator YawRotation(0, Rotation.Yaw, 0);

			//Obtener el vector hacia la derecha 
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

			//A�adir movimiento a esa direccion
			AddMovementInput(Direction, AxisValue);
			return;
		}
	}
}

void APlayerCharacter::MoveUp(float AxisValue) {
	if (bCanGoUp) {
		if (Controller != nullptr && AxisValue != 0.0f) {

			if (!AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_WallRun)) {
				//Buscar que direccion es hacia arriba
				const FRotator Rotation = Controller->GetControlRotation();
				const FRotator YawRotation(0, Rotation.Yaw, 0);

				//Obtener el vector hacia arriba
				const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Z);

				//A�adir movimiento a esa direccion
				AddMovementInput(Direction, AxisValue);
				return;
			}
		}
	}
}

void APlayerCharacter::TurnCamera(float val)
{
	if (!cameraTargetFixComp->GetIsCameraFixed()) {
		AddControllerYawInput(val);
	}
}

void APlayerCharacter::LookUpCamera(float val)
{
	if (!cameraTargetFixComp->GetIsCameraFixed()) {
		AddControllerPitchInput(val);
	}
}

void APlayerCharacter::FixateEnemy()
{
	if (!cameraTargetFixComp->GetIsCameraFixed()) {
		cameraTargetFixComp->TryFixateEnemy();
		
	}
	else {
		cameraTargetFixComp->UnFixateEnemy();
	}
}

void APlayerCharacter::EnergyToZeroCallback()
{
	if (AureiCharacterMovementComponent->IsMovementMode(EMovementMode::MOVE_Flying)
		|| AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_Climb)) {

		bIsGravityEnabled = true;
		AureiCharacterMovementComponent->GravityScale = 1.0f;

		AureiCharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Walking);
	}

}

void APlayerCharacter::RespawnPlayer(EProjectileTypes damageType)
{
	anims->Montage_Play(DeadMontage);

}

void APlayerCharacter::BeginSprint() {
	bPressedSprintBtn = true;

	if (bIsGravityEnabled) {
		if (AureiCharacterMovementComponent->TryWallRun()) {
			AureiCharacterMovementComponent->bOrientRotationToMovement = true; //No sirve para nada hacer esto ;(

		}
		else if(!AureiCharacterMovementComponent->IsFalling()){
			AureiCharacterMovementComponent->MaxWalkSpeed = MaxSpeedRun;
		}
	}
}

void APlayerCharacter::EndSprint() {
	bPressedSprintBtn = false;

	if (bIsGravityEnabled) {
		AureiCharacterMovementComponent->StopWallRuning();
		AureiCharacterMovementComponent->MaxWalkSpeed = NormalSpeedRun;
	}
}

void APlayerCharacter::WhenWREnds() {
	bPressedSprintBtn ? BeginSprint() : EndSprint();
}

void APlayerCharacter::BeginCrouch() {
	if (bIsGravityEnabled) {
		if (AureiCharacterMovementComponent->IsMovementMode(EMovementMode::MOVE_Walking)) {
			if(allowCrouch) Crouch();
		}
		else {
			if (EnergyComp->CanStartSpending(Climb_MinEnergy)) {
				if (AureiCharacterMovementComponent->TryClimb()) {
					EnergyComp->ChangeEnergyState(EEnergyState::Spending, Climb_EnergyCost);
				}
			}
		}
	}
}

void APlayerCharacter::EndCrouch() {
	if (bIsGravityEnabled) {
		UnCrouch();
	}
}

void APlayerCharacter::Fly() {
	if (!allowFly) return;
	//Si la gravedad se ha habilitado, restaura la gravedad del personaje
	if (AureiCharacterMovementComponent->IsMovementMode(EMovementMode::MOVE_Flying)) {
		bIsGravityEnabled = true;

		AureiCharacterMovementComponent->GravityScale = 1.0f;
		AureiCharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Walking);
	}
	else {
		if (EnergyComp->CanStartSpending(Fly_MinEnergy)) {
			EnergyComp->ChangeEnergyState(EEnergyState::Spending, Fly_EnergyCost);

			bIsGravityEnabled = false;

			AureiCharacterMovementComponent->GravityScale = 0.0f;
			AureiCharacterMovementComponent->SetMovementMode(EMovementMode::MOVE_Flying);
		}
	}
}

void APlayerCharacter::Dashing() {
	const FVector UpDir = this->GetActorUpVector();
	LaunchCharacter(UpDir * DashDistance, true, true);
}

void APlayerCharacter::GroundPound() {
	if (AureiCharacterMovementComponent->IsFlying() || AureiCharacterMovementComponent->IsFalling()) {
		FVector characterLocation = GetActorLocation();

		FVector start = characterLocation;
		FVector end = characterLocation - FVector::UpVector * 10000000;
		FHitResult hitResult;
		GetWorld()->LineTraceSingleByChannel(hitResult, start, end, ECC_Visibility, GetIgnoreCharacterParams());

		if (hitResult.bBlockingHit) {
			float DistanceToGround = hitResult.Distance;
			float GroundPoundSpeed = DistanceToGround / TimeGroundPound;

			FVector GroundPoundVelocity = FVector(0, 0, -GroundPoundSpeed);
			LaunchCharacter(GroundPoundVelocity, true, true);
		}
	}
}

void APlayerCharacter::ShootPressed()
{
	ShootingComp->TryShoot();
}

void APlayerCharacter::InteractPressed()
{
	TArray<TEnumAsByte<EObjectTypeQuery> > objects;
	TArray<AActor*> toIgnore;
	toIgnore.Add(this);

	TArray<AActor*> out;

	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), interactRange, objects, AActor::StaticClass(), toIgnore, out);

	for (AActor* actor : out) {
		bool doesImplement = actor->GetClass()->ImplementsInterface(UCanBeInteracted::StaticClass());
		if (doesImplement) {
			ICanBeInteracted::Execute_Interact(actor);
		}
	}
}

void APlayerCharacter::RaiseCharacter() {

	//Si la gravedad no est� activada estamos en este caso
	if (!bIsGravityEnabled) {

		//Establece una altura m�xima a la que se elevar� el personaje
		float maxElevation = MaxElevationFly;

		//Obtiene la posicion del personaje
		FVector characterLocation = GetActorLocation();

		//Encuentra la posici�n del suelo usando un trazado de l�nea hacia abajo desde la posicion actual del personaje 
		FVector start = characterLocation;
		FVector end = characterLocation - FVector::UpVector * maxElevation;
		FHitResult hitResult;
		GetWorld()->LineTraceSingleByChannel(hitResult, start, end, ECC_Visibility, GetIgnoreCharacterParams());

		if (hitResult.bBlockingHit) {
			//Calcula la distancia desde el centro del personaje hasta el suelo
			float distanceToGround = characterLocation.Z - hitResult.ImpactPoint.Z;

			if (distanceToGround <= maxElevation) {

				//Si la distancia al suelo es menor que la altura m�xima, el personaje puede elevarse.
				bCanGoUp = true;
				AureiCharacterMovementComponent->bOrientRotationToMovement = false;
				AureiCharacterMovementComponent->AirControl = 1.0f;
			}
			else {
				//Si llego a el limite se cae al suelo y entra en estado ground
				Fly();
			}
		}
		else {
			//Si no se detecta colision con el suelo
			UE_LOG(LogTemp, Warning, TEXT("No se detect� collision con el suelo."))
				Fly();
		}
	}
	else {
		bCanGoUp = false;
		AureiCharacterMovementComponent->bOrientRotationToMovement = true;

		if (AureiCharacterMovementComponent->MovementMode == MOVE_Falling) {
			AureiCharacterMovementComponent->AirControl = AirControl;
		}
		else {
			AureiCharacterMovementComponent->AirControl = 0.0f;
		}
	}

}

FCollisionQueryParams APlayerCharacter::GetIgnoreCharacterParams() const {

	FCollisionQueryParams Params;

	TArray<AActor*> CharacterChildren;
	GetAllChildActors(CharacterChildren);
	Params.AddIgnoredActors(CharacterChildren);
	Params.AddIgnoredActor(this);

	return Params;
}

void APlayerCharacter::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (bInterrupted) return;


	if (Montage == DeadMontage) {
		SetActorTransform(startingTransform);
		HealthComp->HealDamage(HealthComp->maxHealth);

		if (gm != nullptr) {
			gm->PlayerRespawnEvent.Broadcast();
		}
	}
}