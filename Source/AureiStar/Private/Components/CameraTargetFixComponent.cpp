#include "Components/CameraTargetFixComponent.h"
#include "Camera/CameraComponent.h"
#include "AureiStar/Public/Components/GetCloseEnemies.h"
#include "AureiStar/Public/Enemies/EnemyPawnBase.h"
#include <Kismet/KismetMathLibrary.h>
#include "GameFramework/SpringArmComponent.h"
#include <AureiStar/Public/PlayerCharacter.h>
#include "AureiStar/Public/AureiCharacterMovementComponent.h"

UCameraTargetFixComponent::UCameraTargetFixComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UCameraTargetFixComponent::BeginPlay()
{
	Super::BeginPlay();

	getCloseComp = GetOwner()->GetComponentByClass<UGetCloseEnemies>();
	check(getCloseComp != nullptr);
}

void UCameraTargetFixComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
	isCameraFixed = actorToFixate != nullptr;

	if (isCameraFixed) {
		if (actorToFixate->isDead()) {
			TryFixateEnemy();
		}
		else {
			APlayerCharacter* pl = Cast<APlayerCharacter>(GetOwner());
			APlayerController* con = Cast<APlayerController>(pl->GetController());

			FRotator lookAtRot = UKismetMathLibrary::FindLookAtRotation(GetOwner()->GetActorLocation(), actorToFixate->GetActorLocation());
			FRotator baseRot = con->GetControlRotation();
			FRotator targetRotation = FRotator(lookAtRot.Pitch + pitchOffset, lookAtRot.Yaw, baseRot.Roll);
		
			FRotator deltaRot = FMath::RInterpTo(baseRot, targetRotation, DeltaTime, interSpeed);
			con->SetControlRotation(deltaRot);

			if (updateActorRotation) {
				if (!pl->AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_WallRun) &&
					!pl->AureiCharacterMovementComponent->IsCustomMovementMode(ECustomMovementMode::CMOVE_Climb)
					)
				{
					GetOwner()->SetActorRotation(FRotator(0, lookAtRot.Yaw, baseRot.Roll));
				}
			}
		}
	}
}

void UCameraTargetFixComponent::TryFixateEnemy()
{
	actorToFixate = getCloseComp->getClosestEnemy();
}

void UCameraTargetFixComponent::UnFixateEnemy()
{
	actorToFixate = nullptr;
}

