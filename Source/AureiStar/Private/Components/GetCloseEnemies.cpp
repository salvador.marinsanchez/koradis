
#include "Components/GetCloseEnemies.h"
#include "Components/SphereComponent.h"
#include "AureiStar/Public/Enemies/EnemyPawnBase.h"

UGetCloseEnemies::UGetCloseEnemies()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UGetCloseEnemies::BeginPlay()
{
	Super::BeginPlay();

}

void UGetCloseEnemies::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


	calculateClosestEnemy();
}

void UGetCloseEnemies::SetSphereComp(USphereComponent* sphere)
{
	closeEnemiesSphere = sphere;
	closeEnemiesSphere->OnComponentBeginOverlap.AddDynamic(this, &UGetCloseEnemies::OnSphereBeginOverlap);
	closeEnemiesSphere->OnComponentEndOverlap.AddDynamic(this, &UGetCloseEnemies::OnSphereEndOverlap);

	TArray<UPrimitiveComponent*> output;
	closeEnemiesSphere->GetOverlappingComponents(output);

	for (UPrimitiveComponent* comp : output) {

		if (isPossibleTarget(comp) && !enemiesInRange.Contains(comp->GetOwner())) {
			enemiesInRange.Add(Cast<AEnemyPawnBase>(comp->GetOwner()));
		}
	}
}

void UGetCloseEnemies::OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (isPossibleTarget(OtherComp) && !enemiesInRange.Contains(OtherActor)) {
		enemiesInRange.Add(Cast<AEnemyPawnBase>(OtherActor));
	}
}

void UGetCloseEnemies::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (isPossibleTarget(OtherComp) && enemiesInRange.Contains(OtherActor)) {
		enemiesInRange.Remove(Cast<AEnemyPawnBase>(OtherActor));
	}
}

bool UGetCloseEnemies::isPossibleTarget(UPrimitiveComponent* OtherComp)
{
	return OtherComp->ComponentHasTag("EnemyBody");
}

bool UGetCloseEnemies::isTargetInView(AActor* OtherActor)
{
	FHitResult hit;

	FCollisionQueryParams queryParams;
	queryParams.AddIgnoredActor(GetOwner());
	queryParams.AddIgnoredActor(OtherActor);

	bool isInView = !GetWorld()->LineTraceSingleByChannel(hit, GetOwner()->GetActorLocation(), OtherActor->GetActorLocation(),
		ECollisionChannel::ECC_WorldStatic, queryParams);

	return isInView;
}

void UGetCloseEnemies::calculateClosestEnemy()
{
	AEnemyPawnBase* closestActor = nullptr;

	if (enemiesInRange.Num() > 0) {

		float closestSqrDist = TNumericLimits<float>::Max();

		for (AEnemyPawnBase* enemy : enemiesInRange) {

			if (!enemy->isDead() && isTargetInView(enemy)) {
				float newDist = FVector::DistSquared(GetOwner()->GetActorLocation(), enemy->GetActorLocation());

				if (newDist < closestSqrDist) {
					closestSqrDist = newDist;
					closestActor = enemy;
				}
			}
		}
	}

	closestEnemy = closestActor;
}

