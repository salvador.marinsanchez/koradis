// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/MeleeEnemyController.h"
#include "Enemies/MeleeEnemyPawn.h"
#include "Components/CapsuleComponent.h"

void AMeleeEnemyController::TryAttack(FVector playerLocation, float DeltaTime)
{
	if (TryStartCharge(playerLocation, DeltaTime)) return;

	FVector traceToPlayer = playerLocation - MeleePawn->GetActorLocation();
	if (traceToPlayer.Size() <= MeleePawn->AttackStartDistance) {
		MeleePawn->PrepareComboAttack();
		ChangeBaseState(EEnemyBaseStates::EB_ATTACK);
	}

	return;
}

void AMeleeEnemyController::OnChargeFinished()
{
	ChangeBaseState(EEnemyBaseStates::EB_PLAYER_DETECTED);
	curChargeCD = MeleePawn->ChargeCooldown;
}

void AMeleeEnemyController::OnComboFinished()
{
	ChangeBaseState(EEnemyBaseStates::EB_PLAYER_DETECTED);
}

void AMeleeEnemyController::OnEnemyHit()
{
	if (baseState == EEnemyBaseStates::EB_ATTACK && MeleePawn->getChargeState() != EMelee_Charge::ERA_NOT_CHARGING) return;
	
	ChangeBaseState(EEnemyBaseStates::EB_HIT);
	MeleePawn->anims->StopAllMontages(false);
}

void AMeleeEnemyController::OnEnemyDead()
{
	ChangeBaseState(EEnemyBaseStates::EB_DEAD);
	MeleePawn->SetChargeState(EMelee_Charge::ERA_NOT_CHARGING);

	MeleePawn->anims->StopAllMontages(false);
	StopMovement();

	MeleePawn->OnDead();

}

void AMeleeEnemyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	MeleePawn = Cast<AMeleeEnemyPawn>(InPawn);
	MeleePawn->MeleeChargeFinishedEvent.BindUObject(this, &AMeleeEnemyController::OnChargeFinished);
	MeleePawn->MeleeComboFinishedEvent.BindUObject(this, &AMeleeEnemyController::OnComboFinished);
}


bool AMeleeEnemyController::CanTraceToPlayer(FVector playerLocation)
{
	

	//Check middle trace
	FVector offset = FVector(0, 0, 0);
	if(TraceToPlayer(offset, playerLocation)) return false;

	//Check bottom trace
	offset = FVector(0, 0, -MeleePawn->hitbox->GetUnscaledCapsuleHalfHeight() + 10);
	if (TraceToPlayer(offset, playerLocation)) return false;

	//Check Top trace
	offset = FVector(0, 0, MeleePawn->hitbox->GetUnscaledCapsuleHalfHeight() - 10);
	if (TraceToPlayer(offset, playerLocation)) return false;

	//Check right trace
	offset = MeleePawn->GetActorRightVector() * (MeleePawn->hitbox->GetUnscaledCapsuleRadius() - 1);
	if (TraceToPlayer(offset, playerLocation)) return false;

	//Check left trace
	offset = MeleePawn->GetActorRightVector() * -(MeleePawn->hitbox->GetUnscaledCapsuleRadius() - 1);
	if (TraceToPlayer(offset, playerLocation)) return false;

    return true;
}

//Returns true if its not safe to trace to the player from the enemyPawn. So it returns true if it hits something that is not the player
bool AMeleeEnemyController::TraceToPlayer( FVector& offset, FVector& BaseEnd)
{
	FHitResult hit;
	FCollisionQueryParams queryParams;
	queryParams.AddIgnoredActor(MeleePawn);

	GetWorld()->LineTraceSingleByChannel(hit, MeleePawn->GetActorLocation() + offset, BaseEnd + offset, ECollisionChannel::ECC_Pawn, queryParams);

	bool isUnsafe = hit.bBlockingHit && !hit.GetComponent()->ComponentHasTag("PlayerBody");
	if ( isUnsafe) {
		DrawDebugPoint(GetWorld(), hit.ImpactPoint, 25, FColor::Black, false, 1.f);
		DrawDebugPoint(GetWorld(), hit.GetComponent()->GetComponentLocation(), 25, FColor::Purple, false, 1.f);
	}
	DrawDebugLine(GetWorld(), MeleePawn->GetActorLocation() + offset, BaseEnd + offset, isUnsafe ? FColor::Red : FColor::Green, false, 2.f);
	
	return isUnsafe;
}

bool AMeleeEnemyController::TryStartCharge(FVector playerLocation, float DeltaTime)
{
	if (curChargeCD < 0) {
		curChargeCD = 0;
	}
	else {
		curChargeCD -= DeltaTime;
	}

	if (curChargeCD != 0) return false;

	FVector traceToPlayer = playerLocation - MeleePawn->GetActorLocation();
	if (traceToPlayer.Size() > MeleePawn->ChargeStartDistance) return false;

	if (CanTraceToPlayer(playerLocation)) {
		MeleePawn->PrepareCharge(traceToPlayer.GetSafeNormal());
		ChangeBaseState(EEnemyBaseStates::EB_ATTACK);
		return true;
	}

	return false;
}
