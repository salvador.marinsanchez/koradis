// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/RangedEnemyController.h"
#include "Enemies/RangedEnemyPawn.h"

void ARangedEnemyController::OnEnemyHit()
{
	StopMovement();

	//Ranged attack cant be interrupted by damage
	if (baseState == EEnemyBaseStates::EB_PLAYER_DETECTED && attackState == ERanged_Attack::ERA_LONG) {
		return;
	}

	Super::OnEnemyHit();
}

void ARangedEnemyController::OnEnemyDead()
{
	ChangeBaseState(EEnemyBaseStates::EB_DEAD);

	RangedPawn->anims->StopAllMontages(false);
	StopMovement();

	RangedPawn->OnDead();
}

void ARangedEnemyController::DoAttack()
{
	RangedPawn->DoAttack(attackState == ERanged_Attack::ERA_LONG, locationToShoot);
}

float ARangedEnemyController::StartAimingAttack(FVector locToAim)
{
	locationToShoot = locToAim;

	float distanceToPawn = FVector::Dist(locationToShoot, GetPawn()->GetActorLocation());
	attackState = distanceToPawn <= RangedPawn->rangeToShortAttack ? ERanged_Attack::ERA_SHORT : ERanged_Attack::ERA_LONG;

	float timeToWait = RangedPawn->DoAim(attackState == ERanged_Attack::ERA_LONG);
	
	return timeToWait;
}

void ARangedEnemyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	RangedPawn = Cast<ARangedEnemyPawn>(InPawn);
}
