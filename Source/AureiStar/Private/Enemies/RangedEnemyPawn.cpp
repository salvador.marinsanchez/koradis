// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/RangedEnemyPawn.h"
#include "Projectiles/CustomDamageTypes.h"
#include <Kismet/GameplayStatics.h>
#include <PlayerResources/HealthComponent.h>
#include "GameFramework/FloatingPawnMovement.h"
#include "Components/SkeletalMeshComponent.h"

ARangedEnemyPawn::ARangedEnemyPawn()
{
	mesh = CreateDefaultSubobject<USkeletalMeshComponent>("mesh");
	mesh->SetupAttachment(RootComponent);
}

void ARangedEnemyPawn::DoAttack(bool isLongAttack, FVector targetLocation)
{
	FVector traceEnd = (targetLocation - GetActorLocation()).GetSafeNormal();
	traceEnd *= maxShootRange;
	traceEnd += GetActorLocation();

	FHitResult hit;
	FCollisionQueryParams queryParams;
	queryParams.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(hit, GetActorLocation(), traceEnd, ECollisionChannel::ECC_Pawn, queryParams);

	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	bool isHit = hit.bBlockingHit && hit.GetActor() == playerPawn;

	if (isHit) {
		//Likely can be improved by caching the component or to make UHealthComponent work based on Unreal applyDamage interface so its not necesary to access the component
		UHealthComponent* hpComp = playerPawn->GetComponentByClass<UHealthComponent>();
		hpComp->ReceiveDamage(isLongAttack ? longDamage : shortDamage, EProjectileTypes::GROUND_RANGE);
	}

	DrawDebugLine(GetWorld(), GetActorLocation(), traceEnd, isHit ? FColor::Green : FColor::Red, false, 2.f);
}

void ARangedEnemyPawn::OnReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Cast<UMelee_DamageType>(DamageType) != nullptr) {
		moveComp->MaxSpeed = dashingSpeed;
		isDashing = true;
	}
	else {
		DashFinished();
		TakeDamage(Damage);
	}
}

float ARangedEnemyPawn::DoAim(bool isLongShoot)
{
	UAnimMontage* animToPlay = isLongShoot ? LongAttackMontage : ShortAttackMontage;
	float rate = isLongShoot ? 1 : 0.5;

	return anims->Montage_Play(animToPlay) * rate;
}

void ARangedEnemyPawn::DashFinished()
{
	moveComp->MaxSpeed = baseSpeed;
	isDashing = false;
}

void ARangedEnemyPawn::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (bInterrupted) return;

	if (Montage == DeadMontage) {
		DespawnEnemy();
	}
}

void ARangedEnemyPawn::OnDead()
{
	anims->Montage_Play(DeadMontage);
}

void ARangedEnemyPawn::BeginPlay()
{
	Super::BeginPlay();

	baseSpeed = moveComp->GetMaxSpeed();

	anims = mesh->GetAnimInstance();
	anims->OnMontageEnded.AddDynamic(this, &ARangedEnemyPawn::OnMontageEnded);
}
