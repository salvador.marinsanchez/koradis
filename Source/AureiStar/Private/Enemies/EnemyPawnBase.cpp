#include "Enemies/EnemyPawnBase.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Enemies/PatrolPointBase.h"
#include <AureiStar/AureiStarGameModeBase.h>
#include <Kismet/GameplayStatics.h>

AEnemyPawnBase::AEnemyPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	hitbox = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HitBox"));
	SetRootComponent(hitbox);

	moveComp = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("MovementComponent"));

	OnTakeAnyDamage.AddDynamic(this, &AEnemyPawnBase::OnReceiveDamage);
}

void AEnemyPawnBase::OnReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	TakeDamage(Damage);
}

void AEnemyPawnBase::SpawnEnemy(FVector location, FRotator rotation)
{
	SetActorLocationAndRotation(location, rotation);
	curHp = maxHp;

	SetActorEnableCollision(true);
	SetActorHiddenInGame(false);
	SetActorTickEnabled(true);

	EnemySpawnEvent.Broadcast();
}

void AEnemyPawnBase::DespawnEnemy()
{
	SetActorHiddenInGame(true);
	SetActorEnableCollision(false);
	SetActorTickEnabled(false);

	EnemyDespawnEvent.Broadcast();
}

void AEnemyPawnBase::TakeDamage(float Damage)
{
	curHp -= Damage;

	if (curHp <= 0) {
		curHp = 0;
		EnemyDeadEvent.Broadcast();
	}
	else {
		EnemyHitEvent.Broadcast();
	}
}

void AEnemyPawnBase::OnToAqualisWorld()
{
	if (isDead()) return;

	SpawnEnemy(GetActorLocation(), GetActorRotation());
}

void AEnemyPawnBase::OnToNormalWorld()
{
	if (isDead()) return;

	DespawnEnemy();
}

void AEnemyPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	curHp = maxHp;

	if (startSpawned) {
		SpawnEnemy(GetActorLocation(), GetActorRotation());
	}
	else {
		DespawnEnemy();
	}

	gm = Cast<AAureiStarGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	
	gm->ToAquaWorldEvent.AddDynamic(this, &AEnemyPawnBase::OnToAqualisWorld);
	gm->ToNormalWorldEvent.AddDynamic(this, &AEnemyPawnBase::OnToNormalWorld);

}

void AEnemyPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (orientRotationToMovement) {
		RotateTowardsMovement(DeltaTime);
	}
}

void AEnemyPawnBase::RotateTowardsMovement(float DeltaTime)
{
	FVector velocityV = GetVelocity();
	if (velocityV.IsNearlyZero()) return;

	SetActorRotation(FMath::RInterpTo(GetActorRotation(), velocityV.Rotation(), DeltaTime, RotationSpeed));
	
}

