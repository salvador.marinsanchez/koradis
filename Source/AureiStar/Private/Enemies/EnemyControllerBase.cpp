// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/EnemyControllerBase.h"
#include "Navigation/CrowdFollowingComponent.h"
#include "Enemies/EnemyPawnBase.h"
#include "Perception/AIPerceptionComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Pawn.h"
#include "Enemies/PatrolPointBase.h"

AEnemyControllerBase::AEnemyControllerBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent"))) {

	PrimaryActorTick.bCanEverTick = true;

	CrowdComp = Cast<UCrowdFollowingComponent>(GetPathFollowingComponent());
	perceptionComp = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("PerceptionComponent"));

	perceptionComp->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyControllerBase::OnTargetPerceptionUpdated);
}

void AEnemyControllerBase::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	enemyPawnBase = Cast<AEnemyPawnBase>(InPawn);

	enemyPawnBase->EnemyHitEvent.AddDynamic(this, &AEnemyControllerBase::OnEnemyHit);
	enemyPawnBase->EnemyDeadEvent.AddDynamic(this, &AEnemyControllerBase::OnEnemyDead);
	enemyPawnBase->EnemySpawnEvent.AddUObject(this, &AEnemyControllerBase::OnEnemySpawn);
	enemyPawnBase->EnemyDespawnEvent.AddUObject(this, &AEnemyControllerBase::OnEnemyDespawn);
}

void AEnemyControllerBase::DoAttack()
{
	//SHOULD BE OVERWRITTEN BY SUBCLASES THAT WANT TO USE THIS FUNCTION
	check(false);
}

void AEnemyControllerBase::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus) {

	if ( Actor != UGameplayStatics::GetPlayerPawn(GetWorld(), 0)) return;
	if (baseState == EEnemyBaseStates::EB_DESPAWNED || baseState == EEnemyBaseStates::EB_DEAD) return;

	isPlayerDetected = Stimulus.IsActive();

	if (isPlayerDetected) {
		ChangeBaseState( EEnemyBaseStates::EB_PLAYER_DETECTED );
	}
	else {
		lastPlayerSeenLocation = Actor->GetActorLocation();
		lastPlayerSeenForward = Actor->GetActorForwardVector();
	}
}

void AEnemyControllerBase::OnEnemyHit() {
	ChangeBaseState(EEnemyBaseStates::EB_HIT);
}
void AEnemyControllerBase::OnEnemyDead() {
	ChangeBaseState(EEnemyBaseStates::EB_DEAD);
}
void AEnemyControllerBase::OnEnemySpawn() {
	currentPatrolPoint = enemyPawnBase->startingPatrolPoint;
	if (currentPatrolPoint >= enemyPawnBase->patrolPoints.Num()) {
		currentPatrolPoint = 0;
	}

	ChangeBaseState(EEnemyBaseStates::EB_PATROL);
	
	SetActorTickEnabled(true);

	perceptionComp->Activate();
}
void AEnemyControllerBase::OnEnemyDespawn() {
	ChangeBaseState(EEnemyBaseStates::EB_DESPAWNED);

	perceptionComp->Deactivate();
	SetActorTickEnabled(false);
}


void AEnemyControllerBase::ChangeBaseState(EEnemyBaseStates newState) {
	check(newState != EEnemyBaseStates::EB_INVALID);

	baseState = newState;
}

bool AEnemyControllerBase::DoesPatrolExist()
{
	return enemyPawnBase->patrolPoints.Num() != 0;
}

FVector AEnemyControllerBase::GetCurrentPatrolPoint()
{
	return enemyPawnBase->patrolPoints[currentPatrolPoint]->GetActorLocation();
}

float AEnemyControllerBase::GetCurrentPatrolPointWait()
{
	return enemyPawnBase->patrolPoints[currentPatrolPoint]->timeToWaitHere;
}

void AEnemyControllerBase::NextPatrolPoint()
{
	if (enemyPawnBase->patrolPoints[currentPatrolPoint]->changeDirection) {
		patrolState = patrolState == EPatrolState::EBP_INCREASING ? EPatrolState::EBP_DECREASING : EPatrolState::EBP_INCREASING;
	}

	if (patrolState == EPatrolState::EBP_INCREASING) {
		++currentPatrolPoint;
		if (currentPatrolPoint >= enemyPawnBase->patrolPoints.Num()) {
			currentPatrolPoint = 0;
		}
	}
	else if (patrolState == EPatrolState::EBP_DECREASING){
		--currentPatrolPoint;

		if (currentPatrolPoint < 0) {
			currentPatrolPoint = enemyPawnBase->patrolPoints.Num() - 1;
		}
	}
}
