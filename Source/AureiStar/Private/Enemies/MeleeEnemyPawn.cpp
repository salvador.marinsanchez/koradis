// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies/MeleeEnemyPawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Curves/CurveFloat.h"
#include <PlayerResources/HealthComponent.h>
#include "Components/PrimitiveComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/BoxComponent.h"
#include "AureiStar/Public/PlayerCharacter.h"
#include "Components/SkeletalMeshComponent.h"

AMeleeEnemyPawn::AMeleeEnemyPawn()
{
	mesh = CreateDefaultSubobject<USkeletalMeshComponent>("mesh");
	mesh->SetupAttachment(RootComponent);

	weakAttackHitbox = CreateDefaultSubobject<UBoxComponent>("WeakAttackHitbox");
	weakAttackHitbox->SetupAttachment(RootComponent);

	strongAttackHitbox = CreateDefaultSubobject<UBoxComponent>("strongAttackHitbox");
	strongAttackHitbox->SetupAttachment(RootComponent);
}

void AMeleeEnemyPawn::PrepareComboAttack()
{
	attackCount = 0;
	moveComp->StopMovementImmediately();

	DoAttack();
}

void AMeleeEnemyPawn::DoAttack()
{
	anims->Montage_Play(hasStrongAttackCharged ? StrongAttackMontage : WeakAttackMontage);
}

void AMeleeEnemyPawn::DoDamage()
{
	UBoxComponent* boxToCheck = hasStrongAttackCharged ? strongAttackHitbox : weakAttackHitbox;
	
	TArray<UPrimitiveComponent*> out;
	boxToCheck->GetOverlappingComponents(out);

	for (UPrimitiveComponent* comp : out) {
		if (comp->ComponentHasTag("PlayerBody")) {

			UHealthComponent* hpComp = comp->GetOwner()->GetComponentByClass<UHealthComponent>();
			hpComp->ReceiveDamage(hasStrongAttackCharged ? strongDamage : weakDamage, EProjectileTypes::GROUND_MELEE);

			break;
		}
	}

	DrawDebugBox(GetWorld(), boxToCheck->GetComponentLocation(), boxToCheck->GetScaledBoxExtent(), hasStrongAttackCharged ? FColor::Red : FColor::Green, false, 0.2f, 0, 5.f);

	hasStrongAttackCharged = false;
}

void AMeleeEnemyPawn::AttackFinished()
{
	attackCount++;

	if (attackCount > 2) {
		MeleeComboFinishedEvent.ExecuteIfBound();
	}
	else {
		DoAttack();
	}
}

void AMeleeEnemyPawn::DoCharge(float DeltaTime)
{
	float distanceAlpha = (GetActorLocation() - chargeStartLocation).Size() / ChargeDistance;
	float realChargeSpeed = ChargeCurve->GetFloatValue(distanceAlpha) * ChargingSpeed;

	if (distanceAlpha >= 1) {
		ChargeFinished();
		return;
	}

	bool endCharge = false;

	FHitResult hit;
	SetActorLocation(GetActorLocation() + (chargeDirection * DeltaTime * realChargeSpeed), true, &hit);

	if (hit.bBlockingHit) {
		endCharge = true;

		if (hit.GetComponent()->ComponentHasTag("PlayerBody") && !hasDamagedPlayer) {
			hasDamagedPlayer = true;

			APlayerCharacter* playerChar = Cast<APlayerCharacter>(hit.GetActor());
			playerChar->HealthComp->ReceiveDamage(ChargeDamage, EProjectileTypes::GROUND_MELEE);

			FVector impulseDir = (playerChar->GetActorLocation() - GetActorLocation()).GetSafeNormal();;
			
			playerChar->LaunchCharacter(impulseDir * ChargeKnockback, true, true);

		}
	}

	if (endCharge) { ChargeFinished(); return; }
}

void AMeleeEnemyPawn::ChargeFinished()
{
	moveComp->MaxSpeed = baseSpeed;
	
	SetChargeState(EMelee_Charge::ERA_NOT_CHARGING);
	hasDamagedPlayer = false;

	MeleeChargeFinishedEvent.ExecuteIfBound();
}

void AMeleeEnemyPawn::PrepareCharge(FVector chargeDir)
{
	moveComp->StopMovementImmediately();
	moveComp->MaxSpeed = ChargingSpeed;

	SetChargeState(EMelee_Charge::ERA_CHARGE_READY);

	chargeDir.Z = 0;
	chargeDirection = chargeDir;
	

	chargeStartLocation = GetActorLocation();

	hasDamagedPlayer = false;
	hasStrongAttackCharged = true;


	anims->Montage_Play(ChargePrepMontage);
}

void AMeleeEnemyPawn::SetChargeState(EMelee_Charge newState) {
	chargeState = newState;
}


void AMeleeEnemyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (chargeState == EMelee_Charge::ERA_CHARGING) {
		DoCharge(DeltaTime);
	}

	FVector previousV = moveComp->Velocity;
	FVector newVeloc = previousV + FVector(0, 0, -gravityStrength * DeltaTime);

	moveComp->Velocity = newVeloc;
	moveComp->UpdateComponentVelocity();
}

void AMeleeEnemyPawn::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (bInterrupted) return;

	if (Montage == StrongAttackMontage ||
		Montage == WeakAttackMontage) {

		AttackFinished();
	}
	else if (Montage == ChargePrepMontage) {
		SetChargeState(EMelee_Charge::ERA_CHARGING);
	}
	else if (Montage == DeadMontage) {
		DespawnEnemy();
	}
}

void AMeleeEnemyPawn::OnDead()
{
	anims->Montage_Play(DeadMontage);
}

void AMeleeEnemyPawn::BeginPlay()
{
	Super::BeginPlay();

	anims = mesh->GetAnimInstance();
	anims->OnMontageEnded.AddDynamic(this, &AMeleeEnemyPawn::OnMontageEnded);

	baseSpeed = moveComp->GetMaxSpeed();
}
