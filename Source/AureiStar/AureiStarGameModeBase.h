// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AureiStarGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FToAquaWorld);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FToNormalWorld);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerRespawn);

UCLASS()
class AUREISTAR_API AAureiStarGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable) FToAquaWorld ToAquaWorldEvent;
	UPROPERTY(BlueprintAssignable) FToAquaWorld ToNormalWorldEvent;
	UPROPERTY(BlueprintAssignable) FPlayerRespawn PlayerRespawnEvent;

private:
	bool isAqualisWorld = false;


public:
	UFUNCTION(BlueprintCallable) void WorldChanged(bool toAqualis);

	UFUNCTION(BlueprintCallable) bool IsAqualisWorld() {return isAqualisWorld;}


};
