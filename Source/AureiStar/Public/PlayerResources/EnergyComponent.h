/*
The performance of this component could probably be improved by using tick function and a counter instead of abusing
setTimers, but the player is the only one using it, so i doubt it matters much in the long run. 
*/
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnergyComponent.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEnergyChangedEvent, float, oldEn, float, newEn);

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FZeroEnergyEvent);

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSpendingDisabled);

UENUM(BlueprintType)
enum EEnergyState
{
	Idle,
	Regenerating,
	Spending,
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AUREISTAR_API UEnergyComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	//-- Delegates --//
	UPROPERTY(BlueprintAssignable, Category = "Energy|Events")
	FEnergyChangedEvent energyChangedEvent;

	UPROPERTY(BlueprintAssignable, Category = "Energy|Events")
	FZeroEnergyEvent zeroEnergyEvent;

	UPROPERTY(BlueprintAssignable, Category = "Energy|Events")
	FSpendingDisabled spendingDisabledEvent;
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Energy") float maxEnergy = 100;
	UPROPERTY(BlueprintReadOnly, Category = "Energy") float currentEnergy = 0;
	
	UPROPERTY(BlueprintReadOnly, Category = "Energy") TEnumAsByte<EEnergyState> state = EEnergyState::Idle;

	//If true, after expending energy and a delay, energy will automaticaly be restored
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Energy|Regen") bool isAutoRegenActive = true;
	//Amount of energy regenerated per second with autoregen
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Energy|Regen", meta = ( Units = "s")) float AutoRegenEnergy = 10.f;
	//Amount of energy regenerated per second. This value should always be positive, in case the state is spending, it will be negated from code
	UPROPERTY(BlueprintReadOnly, Category = "Energy|Regen", meta = (Units = "s")) float currentEnergyRegen = 1.f;
	//If isRegenActive, delay between the last energy expense and the start of the regeneration
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Energy|Regen", meta = (ClampMin = 0.05f, Units = "s")) float delayToStartAutoRegen = 2.f;

private:
	FTimerHandle autoRegenHandle;
	bool disableSpending = false;

public:
	UEnergyComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void ChangeEnergyState(EEnergyState newState, float newEnergyRegen = 0);
	UFUNCTION(BlueprintCallable)
	void SetAutoRegen(bool newAutoRegen);

	UFUNCTION(BlueprintCallable)
	bool CanStartSpending(float minEnergyTo);

	UFUNCTION(BlueprintCallable)
	void SetDisableSpending(bool disable);

private:
	void ChangeEnergy(float amountToChange);

	void tryAutoRegen();
};
