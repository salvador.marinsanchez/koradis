﻿// Can be improved to make it work based on Unreal applyDamage / damage types utilities

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AureiStar/Public/Projectiles/Actors/ProjectileBase.h"

#include "HealthComponent.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHealthChangedEvent, float, oldHp, float, newHp);

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FZeroHealthEvent, EProjectileTypes, damageType);


UENUM(BlueprintType)
enum EHealthState
{
	stable,
	increasing,
	reducing
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class AUREISTAR_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	//-- Delegates --//
	UPROPERTY(BlueprintAssignable, Category = "Health|Events")
	FHealthChangedEvent healthChangedEvent;

	UPROPERTY(BlueprintAssignable, Category = "Health|Events")
	FZeroHealthEvent zeroHealthEvent;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Health") float maxHealth = 100;
	UPROPERTY(BlueprintReadOnly, Category = "Health") float currentHealth = 0;

	UPROPERTY(BlueprintReadOnly, Category = "Health") TEnumAsByte<EHealthState> state = EHealthState::stable;
	UPROPERTY(EditAnyWhere, Category = "Health", BlueprintReadWrite) TMap<EProjectileTypes, float> damageVulnerability;

	//If true, after receiving damage and a delay, health will automaticaly be restored
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health|Regen") bool isAutoRegenActive = true;
	//Amount of health regenerated per second with autoregen
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health|Regen", meta = (Units = "s")) float AutoRegenHealth = 10.f;
	//Amount of health regenerated per second. This value should always be positive, in case the state is spending, it will be negated from code
	UPROPERTY(BlueprintReadOnly, Category = "Health|Regen", meta = (Units = "s")) float currentHealthRegen = 1.f;
	//If isRegenActive, delay between the last damage received expense and the start of the regeneration
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Health|Regen", meta = (ClampMin = 0.05f, Units = "s")) float delayToStartAutoRegen = 2.f;

private:
	FTimerHandle autoRegenHandle;
	EProjectileTypes currentRegenDamageType;

public:
	UHealthComponent();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void ChangeHealthState(EHealthState newState, EProjectileTypes damageType = EProjectileTypes::GROUND_MELEE, float newHealthRegen = 0);
	UFUNCTION(BlueprintCallable)
	void SetAutoRegen(bool newAutoRegen);

	UFUNCTION(BlueprintCallable)
	void ReceiveDamage(float amount, EProjectileTypes type = EProjectileTypes::GROUND_MELEE);

	UFUNCTION(BlueprintCallable)
	void HealDamage(float amount);

private:
	void ChangeHealth(float amountToChange, EProjectileTypes type);

	void tryAutoRegen();
};
