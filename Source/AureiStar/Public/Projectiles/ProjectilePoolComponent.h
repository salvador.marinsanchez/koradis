// Holds a pool of every available projectile.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AureiStar/Public/Projectiles/Actors/ProjectileBase.h"
#include "ProjectilePoolComponent.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AUREISTAR_API UProjectilePoolComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UPROPERTY(EditAnyWhere, Category = "ProjectilePool", BlueprintReadWrite) TMap<EProjectileTypes, int> initialPoolSize;
	UPROPERTY(EditAnyWhere, Category = "ProjectilePool", BlueprintReadWrite) TMap<EProjectileTypes, TSubclassOf<AProjectileBase>> enumToClass;

private:
	//One array of each projectile is necesary
	TArray<class AProjClimb*> Climbs;
	TArray<class AProjFlyingMelee*> FlyMelees;
	TArray<class AProjFlyingRange*> FlyRanges;
	TArray<class AProjGroundMelee*> GroundMelees;
	TArray<class AProjGroundRange*> GroundRanges;
	TArray<class AProjWallRun*> WallRuns;

public:
	UProjectilePoolComponent();

	//Returns a projectile. If there are no projectiles in the pool, it will create a new one
	AProjectileBase* GetProjectile(EProjectileTypes type);
	//Adds an already created projectile to the pool
	void AddProjectile(AProjectileBase* actor, EProjectileTypes type);

protected:
	virtual void BeginPlay() override;
		

private:
	//Update enumToClass array with the default projectile classes in case they are not correctly initialized	
	void initClasses();
	//Starts the pools of each projectile with a base numer of projectiles
	void initPools();
	
	void CreateProjectile(EProjectileTypes type);

	//Aux function to templatize getProjectile with any of TArray
	AProjectileBase* GetOrCreateProjectile(EProjectileTypes type, TArray< AProjectileBase*>& arr);

};
