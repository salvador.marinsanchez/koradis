/*
- Honing projectile
- Shoot projectiles that deals AoE damage once they hit an enemy or any other actor.
- Damage is reduced based on distance to impact point
*/
#pragma once

#include "CoreMinimal.h"
#include "Projectiles/Actors/ProjectileBase.h"
#include "ProjClimb.generated.h"

/**
 * 
 */
UCLASS()
class AUREISTAR_API AProjClimb : public AProjectileBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* sphere;

	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float AoEradius = 250.F;
	UPROPERTY(EditDefaultsOnly, Category = "Projectile") UCurveFloat* RadiusDamageFallOffCurve;
public:
	AProjClimb();

	virtual void Shoot(AActor* target, FVector direction) override;

	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void BeginPlay() override;

private:

	void doAoE(AActor* hitActor);
};
