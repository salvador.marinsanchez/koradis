/*
- Hones on the target on shoot, but after that it mantains the same direction
- Doesnt stop when hitting an enemy.
- There is a damage fallOff when hitting different enemies
- Cant damage the same enemy several times
*/
#pragma once

#include "CoreMinimal.h"
#include "Projectiles/Actors/ProjectileBase.h"
#include "ProjGroundRange.generated.h"


UCLASS()
class AUREISTAR_API AProjGroundRange : public AProjectileBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* sphere;

	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float damageDropOffmultiplier = 0.80F;
private:
	float initialBaseDamage;

	TArray<AActor*> actorsHit;

public:
	AProjGroundRange();

	virtual void Shoot(AActor* target, FVector direction) override;

	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void BeginPlay() override;
};
