/*
- Projectile that advances in the actor forward direction
- Doesnt stop when hitting an enemy.
- There is no damage fallOff when hitting different enemies
- Cant damage the same enemy several times
*/
#pragma once

#include "CoreMinimal.h"
#include "Projectiles/Actors/ProjectileBase.h"
#include "ProjGroundMelee.generated.h"

/**
 * 
 */
UCLASS()
class AUREISTAR_API AProjGroundMelee : public AProjectileBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* sphere;

private:
	TArray<AActor*> actorsHit;

public: 
	AProjGroundMelee();

	virtual void Shoot(AActor* target, FVector direction) override;

	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void BeginPlay() override;
};
