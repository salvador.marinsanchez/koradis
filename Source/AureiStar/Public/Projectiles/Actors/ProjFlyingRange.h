/*
- Honing projectile
- Shoot projectiles that debufs the enemy for Xs.
- The debuff deals damage every Xs
- Damage is reduced based on distance to impact point

TODO:
- The debuff also slows the enemy.
- The debuffs can be stacked up to Xs
- Once the enemy is debuffed, receiving X damage will explode all debuffs dealing X damage per debuff
- Dot damage doesnt count towards the explosion (Dot use UDoT_DamageType to diferenciate it from other types of damage)
*/

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/Actors/ProjectileBase.h"
#include "ProjFlyingRange.generated.h"


UCLASS()
class AUREISTAR_API AProjFlyingRange : public AProjectileBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* sphere;

	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float damageOnTick = 0.5f;
	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float tickDuration = 1.f;
	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float debufDuration = 5.f;

private:
	FTimerHandle dotDurationHandle;
	FTimerHandle dotTickHandle;
	AActor* attachedActor = nullptr;

public:
	AProjFlyingRange();

	virtual void Shoot(AActor* target, FVector direction) override;

	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void debufDurationFinished();

	UFUNCTION()
	void debufTickCallback();

protected:
	virtual void BeginPlay() override;
};

