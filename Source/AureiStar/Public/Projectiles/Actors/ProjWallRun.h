/*
- Honing projectile
- Basic projectile, only does damage on hit and despawns
*/
#pragma once

#include "CoreMinimal.h"
#include "Projectiles/Actors/ProjectileBase.h"
#include "ProjWallRun.generated.h"

/**
 * 
 */
UCLASS()
class AUREISTAR_API AProjWallRun : public AProjectileBase
{
	GENERATED_BODY()
	

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* sphere;

private:
	float initialBaseDamage;

public:
	AProjWallRun();

	virtual void Shoot(AActor* target, FVector direction) override;

	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void BeginPlay() override;
};
