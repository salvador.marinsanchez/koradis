
/*
	Has the basic functionality of a projectile.
	- Hide and removes collision of the projectile when created.
	- Show and reactivates collision of the projectile when shooted
	- Check what is a valid target to deal damage to.
	- Auto despawn projectiles after a fixed time. (This functionality can be removed from child by invalidating the timerHandle)
	- Returns the projectile to its projectilePool and restore the projectile to its default state
*/
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Projectiles/CustomDamageTypes.h>
#include "ProjectileBase.generated.h"

UENUM(BlueprintType)
enum class EProjectileTypes : uint8
{
	CLIMB,
	FLY_MELEE,
	FLY_RANGE,
	GROUND_MELEE,
	GROUND_RANGE,
	WALLRUN,
	HEAL,
	MAX
};

ENUM_RANGE_BY_COUNT(EProjectileTypes, EProjectileTypes::MAX)

UCLASS()
class AUREISTAR_API AProjectileBase : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UProjectileMovementComponent* movComp;

	UPROPERTY(EditAnyWhere, Category = "Projectile|Base", BlueprintReadWrite) float cooldown = 1.f;
	UPROPERTY(EditAnywhere, Category = "Projectile|Base", BlueprintReadWrite) float secondsToDespawn = 5;
	UPROPERTY(EditAnyWhere, Category = "Projectile|Base", BlueprintReadWrite) EProjectileTypes ptype = EProjectileTypes::MAX;

	UPROPERTY(EditAnyWhere, Category = "Projectile|Base", BlueprintReadWrite, meta = (AllowedClasses = "Custom_DamageType")) TSubclassOf<UDamageType> damageType = URanged_DamageType::StaticClass();
	
	UPROPERTY(EditAnywhere, Category = "Projectile|Base", BlueprintReadWrite) float baseDamage = 1.f;
protected:
	FTimerHandle despawnTimerHandle;

private:
	class UProjectilePoolComponent* pool;

public:
	AProjectileBase();

protected:
	virtual void BeginPlay() override;

	bool isValidTarget(class UPrimitiveComponent *comp, AActor* actor);

public:	
	virtual void Tick(float DeltaTime) override;

	float getCooldown();

	//Shoot at either an actor target or a direction
	virtual void Shoot(AActor *target, FVector direction);

	void SetPool(class UProjectilePoolComponent *pl);

	virtual void ReturnToPool(EProjectileTypes type);

	UFUNCTION()
	void timerRunsOut();
};
