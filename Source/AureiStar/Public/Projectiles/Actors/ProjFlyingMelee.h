/*
- Honing projectile
- Shoot projectiles that stick to the enemy. 
- Once sticked, after Xs they explode dealing damage and increasing the damage of other projectiles of the same type
- After cooldown * 2 seconds without any projectile exploding, resets the damage increase to 0.
*/

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/Actors/ProjectileBase.h"
#include "ProjFlyingMelee.generated.h"

UCLASS()
class AUREISTAR_API AProjFlyingMelee : public AProjectileBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class UStaticMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* sphere;

	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float damageIncreasePerShot = 1.f;
	UPROPERTY(EditAnywhere, Category = "Projectile", BlueprintReadWrite) float timeToExplode = 1.f;

private:
	FTimerHandle explosionTimerHandle;
	AActor* attachedActor = nullptr;

public:
	AProjFlyingMelee();

	virtual void Shoot(AActor* target, FVector direction) override;

	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void ExplosionTimerRunsOut();

protected:
	virtual void BeginPlay() override;


};
