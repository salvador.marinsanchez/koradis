// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "CustomDamageTypes.generated.h"

UCLASS()
class AUREISTAR_API UCustom_DamageType : public UDamageType
{
	GENERATED_BODY()
};

UCLASS()
class AUREISTAR_API UDoT_DamageType : public UCustom_DamageType
{
	GENERATED_BODY()
};

UCLASS()
class AUREISTAR_API UMelee_DamageType : public UCustom_DamageType
{
	GENERATED_BODY()
};

UCLASS()
class AUREISTAR_API URanged_DamageType : public UCustom_DamageType
{
	GENERATED_BODY()
};