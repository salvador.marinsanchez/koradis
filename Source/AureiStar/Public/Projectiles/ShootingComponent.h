/* 
	This component controls when and what does the player shoots.
	It also checks what enemies are nearby to be shooted at.
*/
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AureiStar/Public/AureiCharacterMovementComponent.h"

#include "ShootingComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UShootingComponent : public UActorComponent
{
	GENERATED_BODY()

	class UProjectilePoolComponent *projPool;
	class UAureiCharacterMovementComponent* moveComp;
	class UGetCloseEnemies* getCloseComp;
	class UCameraTargetFixComponent* cameraFixComp;

	class USphereComponent* sphereComp;
public:	

	UPROPERTY(EditAnyWhere, Category = "Shooting Component", BlueprintReadWrite) float distanceToRangeAttack = 500.f;
	UPROPERTY(EditAnyWhere, Category = "Shooting Component", BlueprintReadWrite) bool automaticShoot = true;
	UPROPERTY(EditAnyWhere, Category = "Shooting Component|Cooldown", BlueprintReadWrite) TMap<TEnumAsByte<EMovementMode>, float> MovModeChange_Cooldown;
	UPROPERTY(EditAnyWhere, Category = "Shooting Component|Cooldown", BlueprintReadWrite) TMap<TEnumAsByte<ECustomMovementMode>, float> CustomMovModeChange_Cooldown;

private:
	bool canShoot = true;

	FTimerHandle cooldownTimerHandle;

	TArray<class AEnemyPawnBase*> enemiesInRange;

public:
	UShootingComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void onMovementStateChanged(EMovementMode moveMode, ECustomMovementMode custMode);

	UFUNCTION()
	void TryShoot();

private:
	//Check if the player can shoot. Currently only checks canShoot parameter, but later down the line may have more limitations
	bool isShootAvailable();

	//Disable shooting for cd seconds
	void startCooldown(float cd);
};
