// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class AUREISTAR_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

//Components
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) class UEnergyComponent* EnergyComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) class UHealthComponent* HealthComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) class UGetCloseEnemies* getCloseComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) class UCameraTargetFixComponent* cameraTargetFixComp;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement) UAureiCharacterMovementComponent* AureiCharacterMovementComponent;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) class UShootingComponent* ShootingComp;



	UPROPERTY(EditAnywhere, BlueprintReadWrite) class USphereComponent* EnemiesInRangeSphere;

private:
	//Spring Arm Component para que la c�mara persiga detras al player
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta=(AllowPrivateAccess = "true")) class USpringArmComponent* CameraBoom;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true")) class UCameraComponent* FollowCamera;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Input) float TurnRateGamepad;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite) bool allowCrouch = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite) bool allowFly = false;
public:
	bool bPressedAureiJump;
	bool bPressedSprintBtn;

	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite) float MaxElevationFly;
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite) float DashDistance = 2000;
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite) float AirControl = 0.5f;
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite) float TimeGroundPound = 0.5f;
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite) float WallRunDeceleration = 2;

	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite) float interactRange = 500.f;

	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties|Energy", BlueprintReadWrite) float Fly_EnergyCost = 15.f;
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties|Energy", BlueprintReadWrite) float Fly_MinEnergy = 50.f;

	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties|Energy", BlueprintReadWrite) float Climb_EnergyCost = 10.f;
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties|Energy", BlueprintReadWrite) float Climb_MinEnergy = 10.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PlayerCharacter Properties|Anims") UAnimMontage* DeadMontage;
private:
	FTransform startingTransform;
	UAnimInstance* anims;
	class AAureiStarGameModeBase* gm;

public:
	APlayerCharacter(const FObjectInitializer& ObjectInitializer);
	
 
	virtual void Jump() override;
	virtual void StopJumping() override;

//Input
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite) bool bIsGravityEnabled;

	bool bCanGoUp;

private:
	//Llamado para adelante/atras inputs
	void MoveForward(float InputAxis);

	//Llamado para delante/derecha inputs
	void MoveRight(float InputAxis);

	//Llamado para arriba/abajo inputs
	void MoveUp(float InputAxis);

	void TurnCamera(float val);
	void LookUpCamera(float val);

	//Z target
	void FixateEnemy();

	//Vuela
	void Fly();

	//Esta funcion verifica si hemos llegado al limite de cuanto se puede elevar.
	void RaiseCharacter();

	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	void Dashing();

	void GroundPound();

	void ShootPressed();

	void InteractPressed();
//APawn interface
protected:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE UCameraComponent* GetFollowCamera() const { return FollowCamera; }
	UFUNCTION(BlueprintPure) FORCEINLINE UAureiCharacterMovementComponent* GetAureiCharacterMovement() const { return AureiCharacterMovementComponent; }

	FCollisionQueryParams GetIgnoreCharacterParams() const;

public:
	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite)
	float MaxSpeedRun;

	UPROPERTY(EditAnyWhere, Category = "PlayerCharacter Properties", BlueprintReadWrite)
	float NormalSpeedRun;

	UFUNCTION()
	void EnergyToZeroCallback();

	UFUNCTION() void RespawnPlayer(EProjectileTypes damageType);

protected:
	//Le pone los valores al personaje de velocidad a los de correr
	void BeginSprint();

	//Le pone los valores al personaje de velocidad a los de default
	void EndSprint();

	//Se agacha
	void BeginCrouch();

	//Se para
	void EndCrouch();

	UFUNCTION()
	void WhenWREnds();

	UFUNCTION()
	void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);
};
