#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CanBeInteracted.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UCanBeInteracted : public UInterface
{
	GENERATED_BODY()
};

class AUREISTAR_API ICanBeInteracted
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Interact();
};