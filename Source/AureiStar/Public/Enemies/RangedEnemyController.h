#pragma once

#include "CoreMinimal.h"
#include "Enemies/EnemyControllerBase.h"
#include "RangedEnemyController.generated.h"

//Enemy ranged attack states.
UENUM(BlueprintType)
enum ERanged_Attack
{
	ERA_LONG,	//Doing long attack anim/attack
	ERA_SHORT,  //Doing short attack anim/attack
	ERA_MAX_STATES,  
	ERA_INVALID
};

UCLASS()
class AUREISTAR_API ARangedEnemyController : public AEnemyControllerBase
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyCtrl") class ARangedEnemyPawn* RangedPawn;

private:
	ERanged_Attack attackState = ERanged_Attack::ERA_INVALID;
	FVector locationToShoot;

public:
	void OnEnemyHit();
	void OnEnemyDead();

	void DoAttack();

	//Called from the BTT
	UFUNCTION(BlueprintCallable) float StartAimingAttack(FVector locToAim);

protected:
	virtual void OnPossess(APawn* InPawn) override;

};
