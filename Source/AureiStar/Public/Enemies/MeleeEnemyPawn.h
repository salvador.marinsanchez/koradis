#pragma once

#include "CoreMinimal.h"
#include "Enemies/EnemyPawnBase.h"
#include "MeleeEnemyPawn.generated.h"

UENUM(BlueprintType)
enum EMelee_Charge
{
	ERA_CHARGE_READY,
	ERA_CHARGING,  
	ERA_NOT_CHARGING
};

DECLARE_DELEGATE(FMeleeChargeFinished);
DECLARE_DELEGATE(FMeleeComboFinished);

UCLASS()
class AUREISTAR_API AMeleeEnemyPawn : public AEnemyPawnBase
{
	GENERATED_BODY()

public:
	FMeleeChargeFinished MeleeChargeFinishedEvent;
	FMeleeComboFinished MeleeComboFinishedEvent;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite) class UBoxComponent* weakAttackHitbox;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite) class UBoxComponent* strongAttackHitbox;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite) class USkeletalMeshComponent* mesh;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy") float gravityStrength = 7500.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Attacks") float strongDamage = 20.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Attacks") float weakDamage = 10.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Attacks") float AttackStartDistance = 200.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Anims") UAnimMontage* StrongAttackMontage;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Anims") UAnimMontage* WeakAttackMontage;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Anims") UAnimMontage* ChargePrepMontage;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Anims") UAnimMontage* DeadMontage;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") float ChargeStartDistance = 750.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") float ChargingSpeed = 1000.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") float ChargeDistance = 1000.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") float ChargeDamage = 25.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") float ChargeCooldown = 20.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") float ChargeKnockback = 1500.f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|MeleeEnemy|Charge") class UCurveFloat* ChargeCurve;

	UAnimInstance* anims;
private:
	float baseSpeed;
	FVector chargeDirection;
	FVector chargeStartLocation;

	EMelee_Charge chargeState;

	int attackCount = 0;

	bool hasDamagedPlayer = false;
	bool hasStrongAttackCharged = false;


public:
	AMeleeEnemyPawn();

	UFUNCTION(BlueprintCallable) void PrepareComboAttack();
	void DoAttack();
	UFUNCTION(BlueprintCallable) void DoDamage();
	UFUNCTION(BlueprintCallable) void AttackFinished();

	void DoCharge(float DeltaTime);
	void ChargeFinished();

	void PrepareCharge(FVector chargeDir);

	UFUNCTION(BlueprintCallable) EMelee_Charge getChargeState() { return chargeState; }
	UFUNCTION(BlueprintCallable) void SetChargeState(EMelee_Charge newState);

	void Tick(float DeltaTime) override;

	UFUNCTION()void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION()void OnDead();
protected:
	virtual void BeginPlay() override;
};
