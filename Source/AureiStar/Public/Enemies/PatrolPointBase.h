// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PatrolPointBase.generated.h"

UCLASS()
class AUREISTAR_API APatrolPointBase : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PatrolPoint") float timeToWaitHere = 0.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "PatrolPoint") bool changeDirection = false;

public:	
	APatrolPointBase();
};
