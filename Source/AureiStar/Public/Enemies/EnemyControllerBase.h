// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyControllerBase.generated.h"

//Generic high tier states used by all enemies.
UENUM(BlueprintType)
enum EEnemyBaseStates
{
	EB_PATROL,				//Enemy is not aware of the player
	EB_PLAYER_DETECTED,		//Enemy knows where is the player
	EB_ATTACK,				//Enemy is attacking the player
	EB_HIT,					//Enemy is being hit by something
	EB_DEAD,				//Enemy is dead
	EB_DESPAWNED,			//Enemy is not spawned in the level
	EB_MAX_STATES,
	EB_INVALID
};

//If the patrolling path is being progrsed by increasing or decreasing the patrolPoints array.
UENUM(BlueprintType)
enum EPatrolState
{
	EBP_INCREASING,			
	EBP_DECREASING,
	EBP_SEARCHING_PLAYER,	//Currently unused. It will be used when the enemy has lost sight of the player and will try to look for them 
	EBP_MAX_STATES,
	EBP_INVALID
};


UCLASS()
class AUREISTAR_API AEnemyControllerBase : public AAIController
{
	GENERATED_BODY()
public:
	//Components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyCtrl|Components") class UCrowdFollowingComponent* CrowdComp;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyCtrl|Components") class UAIPerceptionComponent* perceptionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyCtrl") class AEnemyPawnBase* enemyPawnBase;

	//PlayerDetected
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|PlayerDetected") float timeToForgetPlayer = 5.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyPawn|PlayerDetected") bool isPlayerDetected = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyPawn|PlayerDetected") FVector lastPlayerSeenLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyPawn|PlayerDetected") FVector lastPlayerSeenForward;

	
protected:
	EEnemyBaseStates baseState = EEnemyBaseStates::EB_DESPAWNED;

private:
	
	EPatrolState patrolState = EPatrolState::EBP_INCREASING;
	int currentPatrolPoint;

public:
	AEnemyControllerBase(const FObjectInitializer& ObjectInitializer);

	UFUNCTION() virtual void OnEnemyHit();
	UFUNCTION() virtual void OnEnemyDead();
	UFUNCTION() virtual void OnEnemySpawn();
	UFUNCTION() virtual void OnEnemyDespawn();

	UFUNCTION() void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

	//The following (BlueprintCallable) are called from the BTT

	UFUNCTION(BlueprintCallable) EEnemyBaseStates GetBaseState() const { return baseState; }
	UFUNCTION(BlueprintCallable) void ChangeBaseState(EEnemyBaseStates newState);

	//Patrol
	UFUNCTION(BlueprintCallable) bool DoesPatrolExist();
	UFUNCTION(BlueprintCallable) FVector GetCurrentPatrolPoint();
	UFUNCTION(BlueprintCallable) float GetCurrentPatrolPointWait();
	UFUNCTION(BlueprintCallable) void NextPatrolPoint();

	//Attack
	UFUNCTION(BlueprintCallable) virtual void DoAttack();

protected:
	virtual void OnPossess(APawn* InPawn) override;


};
