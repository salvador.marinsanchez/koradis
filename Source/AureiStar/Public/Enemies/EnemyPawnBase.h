// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "EnemyPawnBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemyDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemyHit);
DECLARE_MULTICAST_DELEGATE(FEnemySpawn);
DECLARE_MULTICAST_DELEGATE(FEnemyDespawn);



UCLASS()
class AEnemyPawnBase : public APawn
{
	GENERATED_BODY()

public:
	//Delegates
	UPROPERTY(BlueprintAssignable) FEnemyDead EnemyDeadEvent;
	UPROPERTY(BlueprintAssignable) FEnemyHit EnemyHitEvent;
	FEnemySpawn EnemySpawnEvent;
	FEnemyDespawn EnemyDespawnEvent;

	//Components
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|Components") class UCapsuleComponent* hitbox;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|Components") class UFloatingPawnMovement* moveComp;
	
	//Generic
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "EnemyPawn") bool startSpawned = false;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "EnemyPawn") bool orientRotationToMovement = true;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "EnemyPawn") float RotationSpeed = 100.f;
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|Health") float maxHp = 5.f;

	//Patrol
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|Patrol") TArray<class APatrolPointBase*> patrolPoints;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|Patrol") int startingPatrolPoint;



private:
	float curHp;

	class AAureiStarGameModeBase* gm;
public:
	// Sets default values for this pawn's properties
	AEnemyPawnBase();

	UFUNCTION() virtual void OnReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable) void SpawnEnemy(FVector location, FRotator rotation);
	UFUNCTION(BlueprintCallable) void DespawnEnemy();

	void TakeDamage(float Damage);

	UFUNCTION() void OnToAqualisWorld();
	UFUNCTION() void OnToNormalWorld();

	virtual void Tick(float DeltaTime) override;

	bool isDead() { return curHp <= 0; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	void RotateTowardsMovement(float DeltaTime);

};
