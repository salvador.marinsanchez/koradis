// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemies/EnemyControllerBase.h"
#include "MeleeEnemyController.generated.h"

UCLASS()
class AUREISTAR_API AMeleeEnemyController : public AEnemyControllerBase
{
	GENERATED_BODY()
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyCtrl") class AMeleeEnemyPawn* MeleePawn;

private:
	float curChargeCD = 0;

public:
	UFUNCTION(BlueprintCallable) void TryAttack(FVector playerLocation, float DeltaTime);

	UFUNCTION() void OnChargeFinished();
	UFUNCTION() void OnComboFinished();
	void OnEnemyHit();
	void OnEnemyDead();

protected:
	virtual void OnPossess(APawn* InPawn) override;

private:
	bool CanTraceToPlayer(FVector playerLocation);
	bool TraceToPlayer(FVector& offset, FVector& BaseEnd);
	bool TryStartCharge(FVector playerLocation, float DeltaTime);
};
