#pragma once

#include "CoreMinimal.h"
#include "Enemies/EnemyPawnbase.h"
#include "RangedEnemyPawn.generated.h"


UCLASS()
class AUREISTAR_API ARangedEnemyPawn : public AEnemyPawnBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite) class USkeletalMeshComponent* mesh;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Range") float rangeToShortAttack = 500.f;
	//When the enemy "fires", the shoot will "travel" as far as this parameter. Adding "" because the shoot is instant and doesnt have any projectile
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Range") float maxShootRange = 4000.f;

	//The real aimingTime will be the animation duration + the value of this parameter
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|LongShoot") float longAimTime = 2.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|LongShoot") float longDamage = 20.f;

	//The real aimingTime will be the animation duration + the value of this parameter
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|ShortShoot") float shortAimTime = 1.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|ShortShoot") float shortDamage = 10.f;
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Anims") UAnimMontage* LongAttackMontage;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Anims") UAnimMontage* ShortAttackMontage;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Anims") UAnimMontage* DeadMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "EnemyPawn|RangedEnemy|Dash") bool isDashing = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Dash") float dashingMaxDistance = 1500.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "EnemyPawn|RangedEnemy|Dash") float dashingSpeed = 1000.f;

	UAnimInstance* anims;
private:
	float baseSpeed;

public:
	ARangedEnemyPawn();

	void DoAttack(bool isLongAttack, FVector targetLocation);

	void OnReceiveDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	float DoAim(bool isLongShoot);

	//Called at the end of the dash animation
	UFUNCTION(BlueprintCallable) void DashFinished();

	UFUNCTION()void OnMontageEnded(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION()void OnDead();

protected:
	virtual void BeginPlay() override;
};
