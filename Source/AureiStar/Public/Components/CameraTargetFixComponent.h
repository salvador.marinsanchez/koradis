#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CameraTargetFixComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AUREISTAR_API UCameraTargetFixComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) float interSpeed = 6.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) float pitchOffset = -10.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly) bool updateActorRotation = true;
private:
	class UGetCloseEnemies* getCloseComp;

	bool isCameraFixed = false;
	class AEnemyPawnBase* actorToFixate = nullptr;

public:	
	UCameraTargetFixComponent();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable) bool GetIsCameraFixed() { return isCameraFixed; }
	AEnemyPawnBase* GetFixatedEnemy() { return actorToFixate; }

	void TryFixateEnemy();
	void UnFixateEnemy();
};
