#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GetCloseEnemies.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AUREISTAR_API UGetCloseEnemies : public UActorComponent
{
	GENERATED_BODY()

private:
	class USphereComponent* closeEnemiesSphere;
	TArray<class AEnemyPawnBase*> enemiesInRange;

	class AEnemyPawnBase* closestEnemy;

public:	
	UGetCloseEnemies();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void SetSphereComp(class USphereComponent* sphere);

	AEnemyPawnBase* getClosestEnemy() { return closestEnemy; }


private:

	UFUNCTION() void OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION() void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	bool isPossibleTarget(UPrimitiveComponent* OtherComp);
	bool isTargetInView(AActor* OtherActor);

	void calculateClosestEnemy();


};
